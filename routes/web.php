<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', function () {
    return view('welcome');
});*/

Route::get('/', 'data@index');
//Route::get('/initial', 'data@initial');
Route::get('/start', 'data@start');
Route::get('/menu', 'data@menu');
Route::get('/location', 'data@location');
    
/*Route::get('/Userregister', 'data@Userregister');
Route::get('/Userregisternext', 'data@Userregisternext');
Route::get('/Userregisterend', 'data@Userregisterend');*/
Route::get('/help', 'data@help');
Route::get('/helpnext', 'data@helpnext');
Route::get('/helpnextEl', 'data@helpnextEl');
Route::get('/helpnextIng', 'data@helpnextIng');
Route::get('/helpnextTec', 'data@helpnextTec');
Route::get('/biotec', 'data@biotec');
Route::get('/elect', 'data@elect');
Route::get('/ingd', 'data@ingd');
Route::get('/tecv', 'data@tecv');
Route::get('/tecpark', 'data@tecpark');
Route::get('/services', 'data@services');
Route::get('/project', 'data@project');
Route::get('/Infrastructure', 'data@Infrastructure');
Route::get('/sublines', 'data@sublines');
Route::get('/credits', 'data@credits');

//**************************/
Route::get('/tecpark', 'TechnoparksController@index');
Route::get('/helpnext', 'LinesController@Bio');
Route::get('/helpnextEl', 'LinesController@Elc');
Route::get('/helpnextIng', 'LinesController@Ing');
Route::get('/helpnextTec', 'LinesController@Tec');
Route::get('/sublines', 'SublinesController@index');
Route::get('/location', 'LocationController@index');
Route::get('/services', 'ServicesController@index');
Route::get('/servicesET', 'ServicesETController@index');
Route::get('/servicesID', 'ServicesIDController@index');
Route::get('/servicesTV', 'ServicesTVController@index');
Route::get('/sublinesET', 'SublinesETController@index');
Route::get('/sublinesID', 'SublinesIDController@index');
Route::get('/sublinesTV', 'SublinesTVController@index');
Route::get('/Infrastructure', 'ToolsController@index');
Route::get('/InfrastructureET', 'ToolsETController@index');
Route::get('/InfrastructureID', 'ToolsIDController@index');
Route::get('/InfrastructureTV', 'ToolsTVController@index');
//**************************/
Route::get('/Userregister', 'UserregisterController@createStep1');
Route::post('/Userregister', 'UserregisterController@postCreateStep1');

Route::get('/Userregisternext', 'UserregisterController@createStep2');
Route::post('/Userregisternext', 'UserregisterController@postCreateStep2');

Route::get('/Userregisterend', 'UserregisterController@createStep3');
Route::post('Userregisterend', 'UserregisterController@store');
Route::post('login', 'Auth\LoginController@showLoginForm');

//Route::post('addsolution', 'SolutionsController@store');

Auth::routes();

/**innovacion abierta*/
Route::get('/home', 'HomeController@index')->name('home');
Route::get('/innv', 'InnvController@index')->name('innv');

Route::get('/newret', 'NewretController@index')->name('newret');
Route::post('newret', 'NewretController@store');

Route::get('/viewchallenge', 'NewretController@show')->name('viewchallenge');

Route::get('viewselectchl', 'NewretController@view')->name('viewselectchl');
Route::post('viewselectchl', 'NewretController@view');

Route::get('slnadd', 'SolutionsController@show')->name('slnadd');
Route::post('slnadd', 'SolutionsController@show');


Route::get('aprsln', 'NewretController@edit')->name('aprsln');
Route::post('aprsln', 'NewretController@edit');

Route::get('addsolution', 'SolutionsController@store')->name('addsolution');
Route::post('addsolution', 'SolutionsController@store');

Route::get('otherviewchallenge', 'OtherviewchallengeController@show')->name('otherviewchallenge');
Route::post('otherviewchallenge', 'OtherviewchallengeController@show');

Route::get('viewslnt', 'SolutionsController@viewsln')->name('viewslnt');
Route::post('viewslnt', 'SolutionsController@viewsln');

Route::get('viewslnslt', 'SolutionsController@viewslnselect')->name('viewslnslt');
Route::post('viewslnslt', 'SolutionsController@viewslnselect');

Route::get('viewslnsltusr', 'SolutionsController@viewslnselectusr')->name('viewslnslt');
Route::post('viewslnsltusr', 'SolutionsController@viewslnselectusr');


Route::get('apbslnt', 'SolutionsController@edit')->name('apbslnt');
Route::post('apbslnt', 'SolutionsController@edit');

Route::get('createzipchl/{volume_id}', 'NewretController@chlarchives')->name('createzipchl');
Route::get('create-zip/{volume_id}', 'SolutionsController@createzip')->name('create-zip');