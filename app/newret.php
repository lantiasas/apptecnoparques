<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class newret extends Model
{
    protected $table = 'ats_chl_challenges';

    protected $fillable = [ 'id','name','description','scope','deadline','approved','created_at','idusers'];

    public $timestamps = false;

}
