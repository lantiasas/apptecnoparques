<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Lines extends Model
{
    protected $table = 'ats_lines';

    protected $fillable = [ 'id','name','description'];

    public $timestamps = false;
}
