<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class restriction extends Model
{
    protected $table = 'ats_chl_restrictions';

    protected $fillable = ['id','item','id_challenge'];

    public $timestamps = false;
}
