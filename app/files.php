<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class files extends Model
{
    protected $table = 'ats_chl_files';

    protected $fillable = ['id','name','id_challenge','id_solution'];

    public $timestamps = false;
}
