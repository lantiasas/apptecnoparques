<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Userregister extends Model
{
    protected $table = 'userregisters';

    protected $fillable = [ 'id','name','email','city','departament','redPark','typemembership','typecompany','sectorcompany','help'];

    public $timestamps = false;

    /*protected $table = 'userregisters';

    protected $fillable = [ 'id','name','email','city','departament'];

    public $timestamps = false;*/
}
