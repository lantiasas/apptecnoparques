<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class conditions extends Model
{
    protected $table = 'ats_chl_conditions';

    protected $fillable = ['id','item','id_challenge'];

    public $timestamps = false;
}
