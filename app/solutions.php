<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class solutions extends Model
{
    protected $table = 'ats_chl_solutions';

    protected $fillable = ['id','description','approved','id_challenge','idusers'];

    public $timestamps = false;
}
