<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class contacts extends Model
{
    protected $table = 'ats_chl_contacts';

    protected $fillable = ['id','name','email','telephone','position','id_challenge'];

    public $timestamps = false;
}
