<?php

namespace App\Http\Controllers;

use App\Lines;
use Illuminate\Http\Request;

class LinesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    public function Bio()
    {
        $bio = \DB::table('ats_lines')->select('id','name','description')->get();
        return view('helpnext', compact('bio')); 
    }
    public function Elc()
    {
        $elc = \DB::table('ats_lines')->select('id','name','description')->get();
        return view('helpnextEl', compact('elc')); 
    }
    public function Ing()
    {
        $ing = \DB::table('ats_lines')->select('id','name','description')->get();
        return view('helpnextIng', compact('ing')); 
    }
    public function Tec()
    {
        $tec = \DB::table('ats_lines')->select('id','name','description')->get();
        return view('helpnextTec', compact('tec')); 
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Lines  $lines
     * @return \Illuminate\Http\Response
     */
    public function show(Lines $lines)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Lines  $lines
     * @return \Illuminate\Http\Response
     */
    public function edit(Lines $lines)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Lines  $lines
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Lines $lines)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Lines  $lines
     * @return \Illuminate\Http\Response
     */
    public function destroy(Lines $lines)
    {
        //
    }
}
