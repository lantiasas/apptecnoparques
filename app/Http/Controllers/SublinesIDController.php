<?php

namespace App\Http\Controllers;

use App\SublinesID;
use Illuminate\Http\Request;

class SublinesIDController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $sublinesID = \DB::table('ats_sublines')
            ->join('ats_lines', 'ats_sublines.line_id', '=', 'ats_lines.id')
            ->select('ats_sublines.name','ats_sublines.icon')
            ->where('ats_lines.id', '3')
            ->get();
        return view('sublinesID', compact('sublinesID'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\SublinesID  $sublinesID
     * @return \Illuminate\Http\Response
     */
    public function show(SublinesID $sublinesID)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\SublinesID  $sublinesID
     * @return \Illuminate\Http\Response
     */
    public function edit(SublinesID $sublinesID)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\SublinesID  $sublinesID
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, SublinesID $sublinesID)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\SublinesID  $sublinesID
     * @return \Illuminate\Http\Response
     */
    public function destroy(SublinesID $sublinesID)
    {
        //
    }
}
