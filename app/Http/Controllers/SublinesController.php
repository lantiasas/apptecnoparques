<?php

namespace App\Http\Controllers;

use App\Sublines;
use Illuminate\Http\Request;

class SublinesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
        $sublines = \DB::table('ats_sublines')
            ->join('ats_lines', 'ats_sublines.line_id', '=', 'ats_lines.id')
            ->select('ats_sublines.name','ats_sublines.icon')
            ->where('ats_lines.id', '1')
            ->get();
        return view('sublines', compact('sublines'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Sublines  $sublines
     * @return \Illuminate\Http\Response
     */
    public function show(Sublines $sublines)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Sublines  $sublines
     * @return \Illuminate\Http\Response
     */
    public function edit(Sublines $sublines)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Sublines  $sublines
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Sublines $sublines)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Sublines  $sublines
     * @return \Illuminate\Http\Response
     */
    public function destroy(Sublines $sublines)
    {
        //
    }
}
