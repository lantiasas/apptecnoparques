<?php

namespace App\Http\Controllers;

use App\Transversal_services;
use Illuminate\Http\Request;

class TransversalServicesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Transversal_services  $transversal_services
     * @return \Illuminate\Http\Response
     */
    public function show(Transversal_services $transversal_services)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Transversal_services  $transversal_services
     * @return \Illuminate\Http\Response
     */
    public function edit(Transversal_services $transversal_services)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Transversal_services  $transversal_services
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Transversal_services $transversal_services)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Transversal_services  $transversal_services
     * @return \Illuminate\Http\Response
     */
    public function destroy(Transversal_services $transversal_services)
    {
        //
    }
}
