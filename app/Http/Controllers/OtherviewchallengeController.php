<?php

namespace App\Http\Controllers;

use App\otherviewchallenge;
use Illuminate\Http\Request;
use App\newret;
use App\conditions;
use App\restriction;
use App\contacts;
use App\files;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use app\Http\Requests\retrequest;
use App;
use DB;
class OtherviewchallengeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\otherviewchallenge  $otherviewchallenge
     * @return \Illuminate\Http\Response
     */
    public function show(otherviewchallenge $otherviewchallenge)
    {
        $otherviewchl = DB::table('users')
        ->join('ats_chl_challenges', 'users.id', '=', 'ats_chl_challenges.idusers')
        ->select('ats_chl_challenges.name as nr', 'ats_chl_challenges.id', 'users.name', 
                 'ats_chl_challenges.description', 'ats_chl_challenges.scope', 
                 'ats_chl_challenges.deadline')
        ->where('ats_chl_challenges.approved','=','1')->paginate(10);
        return view('otherviewchallenge')->with('otherviewchl', $otherviewchl);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\otherviewchallenge  $otherviewchallenge
     * @return \Illuminate\Http\Response
     */
    public function edit(otherviewchallenge $otherviewchallenge)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\otherviewchallenge  $otherviewchallenge
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, otherviewchallenge $otherviewchallenge)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\otherviewchallenge  $otherviewchallenge
     * @return \Illuminate\Http\Response
     */
    public function destroy(otherviewchallenge $otherviewchallenge)
    {
        //
    }
}
