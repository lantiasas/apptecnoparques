<?php

namespace App\Http\Controllers;

use App\SublinesET;
use Illuminate\Http\Request;

class SublinesETController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $sublinesET = \DB::table('ats_sublines')
            ->join('ats_lines', 'ats_sublines.line_id', '=', 'ats_lines.id')
            ->select('ats_sublines.name','ats_sublines.icon')
            ->where('ats_lines.id', '2')
            ->get();
        return view('sublinesET', compact('sublinesET'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\SublinesET  $sublinesET
     * @return \Illuminate\Http\Response
     */
    public function show(SublinesET $sublinesET)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\SublinesET  $sublinesET
     * @return \Illuminate\Http\Response
     */
    public function edit(SublinesET $sublinesET)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\SublinesET  $sublinesET
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, SublinesET $sublinesET)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\SublinesET  $sublinesET
     * @return \Illuminate\Http\Response
     */
    public function destroy(SublinesET $sublinesET)
    {
        //
    }
}
