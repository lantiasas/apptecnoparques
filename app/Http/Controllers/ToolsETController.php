<?php

namespace App\Http\Controllers;

use App\ToolsET;
use Illuminate\Http\Request;

class ToolsETController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $InfrastructureET = \DB::table('ats_tools')
        ->join('ats_lines', 'ats_tools.line_id', '=', 'ats_lines.id')
        ->select('ats_tools.name')
        ->where('ats_lines.id', '2')
        ->get();
        return view('InfrastructureET', compact('InfrastructureET'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ToolsET  $toolsET
     * @return \Illuminate\Http\Response
     */
    public function show(ToolsET $toolsET)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ToolsET  $toolsET
     * @return \Illuminate\Http\Response
     */
    public function edit(ToolsET $toolsET)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ToolsET  $toolsET
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ToolsET $toolsET)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ToolsET  $toolsET
     * @return \Illuminate\Http\Response
     */
    public function destroy(ToolsET $toolsET)
    {
        //
    }
}
