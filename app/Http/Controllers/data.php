<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Companies;

class data extends Controller
{
    public function index(){
        return view('index');
    }

    public function initial(){
        return view('initial');
    }

    public function start(){
        return view('start');
    }

    public function menu(){
        return view('menu');
    }

    public function location(){
        return view('location');
    }
    public function help(){
        return view('help');
    }
    public function helpnext(){
        return view('helpnext');
    }
    public function helpnextEl(){
        return view('helpnextEl');
    }
    public function helpnextIng(){
        return view('helpnextIng');
    }
    public function helpnextTec(){
        return view('helpnextTec');
    }
    public function Userregister(){
        return view('Userregister');
    }
    public function Userregisternext(){
        return view('Userregisternext');
    }
    public function Userregisterend(){
        return view('Userregisterend');
    }
    public function biotec(){
        return view('biotec');
    }
    public function elect(){
        return view('elect');
    }
    public function ingd(){
        return view('ingd');
    }
    public function tecv(){
        return view('tecv');
    }
    public function tecpark(){
        return view('tecpark');
    }
    public function services(){
        return view('services');
    }
    public function project(){
        return view('project');
    }
    public function Infrastructure(){
        return view('Infrastructure');
    }
    public function sublines(){
        return view('sublines');
    }
    public function credits(){
        return view('credits');
    }

    public function viewchallenge(){
        return view('viewchallenge');
    }
    public function viewselectchl(){
        return view('viewselectchl');
    }
    public function addsolution(){
        return view('addsolution');
    }
    public function otherviewchallenge(){
        return view('otherviewchallenge');
    }
}
