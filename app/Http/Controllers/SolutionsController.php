<?php

namespace App\Http\Controllers;

use App\solutions;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use app\Http\Requests\retrequest;
use App\files;
use App;
use DB;

class SolutionsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('addsolution');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $addsolutions = new solutions;
        $addsolutions ->description = $request ->descriptionsln;
        $addsolutions ->approved = '0';
        $addsolutions ->id_challenge = $request ->idchls;
        $addsolutions ->idusers = Auth::user()->id;
        $idsln = $request ->idchls;
        $addsolutions->save();
        $solution = DB::table('users')
        ->join('ats_chl_challenges', 'users.id', '=', 'ats_chl_challenges.idusers')
        ->select('users.id as iduser','ats_chl_challenges.name as namechl', 'ats_chl_challenges.id', 'users.name')
        ->where('ats_chl_challenges.id','=',$idsln)
        ->get();
        if($request->hasFile('archivessln')){

            $files = $request->file('archivessln');
            foreach($files as $file){
                $strfile = new files;
                $nombre = $file->getClientOriginalName();
                \Storage::disk('sln')->put($addsolutions->id.'\\'.$nombre,  \File::get($file));
                $strfile->name = $nombre;
                $strfile->id_solution = $addsolutions->id;
                $strfile->save();
            }
        }
        return view('addsolution',compact('solution'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\solutions  $solutions
     * @return \Illuminate\Http\Response
     */
    public function show(Request $Request)
    {
        $solution = DB::table('users')
        ->join('ats_chl_challenges', 'users.id', '=', 'ats_chl_challenges.idusers')
        ->select('users.id as iduser','ats_chl_challenges.name as namechl', 'ats_chl_challenges.id', 'users.name')
        ->where('ats_chl_challenges.id','=',$Request->idviewchl)
        ->get();
        return view('addsolution',compact('solution'));
    }
    
    public function viewsln(Request $Request){

        $solutionview = DB::table('users')
        ->join('ats_chl_solutions', 'users.id', '=', 'ats_chl_solutions.idusers')
        ->select('ats_chl_solutions.id','ats_chl_solutions.approved', 'users.name as username', 'users.rol')
        ->where('ats_chl_solutions.id_challenge','=', $Request->idviewchl)->paginate(10);
            $rol = Auth::user()->rol;
        return view('viewsolutions',["idviewchl" => $Request->idviewchl],compact('solutionview', 'rol'));
    }
    public function viewslnselect(Request $Request){
        
        $solutionsl = DB::table('users')
        ->join('ats_chl_solutions', 'users.id', '=', 'ats_chl_solutions.idusers')
        ->select('ats_chl_solutions.id','ats_chl_solutions.approved', 'ats_chl_solutions.id_challenge' ,'ats_chl_solutions.description' ,'users.name as username', 'users.rol')
        ->where('ats_chl_solutions.id','=',$Request->idsln)
        ->get();

        $files = DB::table('ats_chl_files')
        ->select('id','name','id_solution')
        ->where('id_solution','=',$Request->idsln)
        ->get();
        
        return view('solutionap',compact('solutionsl', 'files'));
    }

    public function viewslnselectusr(Request $Request){
        
        $solutionsl = DB::table('users')
        ->join('ats_chl_solutions', 'users.id', '=', 'ats_chl_solutions.idusers')
        ->select('ats_chl_solutions.id','ats_chl_solutions.approved', 'ats_chl_solutions.id_challenge' ,'ats_chl_solutions.description' ,'users.name as username', 'users.rol')
        ->where('ats_chl_solutions.id','=',$Request->idsln)
        ->get();

        $files = DB::table('ats_chl_files')
        ->select('id','name','id_solution')
        ->where('id_solution','=',$Request->idsln)
        ->get();

        return view('solutionapusr',compact('solutionsl','files'));
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\solutions  $solutions
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $Request)
    {
        $chlEdit = solutions::findOrfail($Request->idchl);
        $chlEdit->approved = '1';
        $chlEdit->save();
        sleep(1);
        $solutionsl = DB::table('users')
        ->join('ats_chl_solutions', 'users.id', '=', 'ats_chl_solutions.idusers')
        ->select('ats_chl_solutions.id','ats_chl_solutions.approved', 'ats_chl_solutions.id_challenge' ,'ats_chl_solutions.description' ,'users.name as username')
        ->where('ats_chl_solutions.id','=',$Request->idchl)
        ->get();
        return view('solutionap',compact('solutionsl'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\solutions  $solutions
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, solutions $solutions)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\solutions  $solutions
     * @return \Illuminate\Http\Response
     */
    public function destroy(solutions $solutions)
    {
        //
    }

    public function createzip($volume_id)
    {
        $zip_file = 'solution.zip';
        $zip = new \ZipArchive();
        $zip->open($zip_file, \ZipArchive::CREATE | \ZipArchive::OVERWRITE);
        
        $path = storage_path('archives/sln/'.$volume_id.'');
        $files = new \RecursiveIteratorIterator(new \RecursiveDirectoryIterator($path));
        foreach ($files as $name => $file)
        {
            if (!$file->isDir()) {
                $filePath = $file->getRealPath();
                $relativePath = substr($filePath, strlen($path) + 1);
        
                $zip->addFile($filePath, $relativePath);
            }
        }
        $zip->close();
        return response()->download($zip_file);
    }
}