<?php

namespace App\Http\Controllers;

use App\ToolsID;
use Illuminate\Http\Request;

class ToolsIDController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $InfrastructureET = \DB::table('ats_tools')
        ->join('ats_lines', 'ats_tools.line_id', '=', 'ats_lines.id')
        ->select('ats_tools.name')
        ->where('ats_lines.id', '3')
        ->get();
        return view('InfrastructureET', compact('InfrastructureET'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ToolsID  $toolsID
     * @return \Illuminate\Http\Response
     */
    public function show(ToolsID $toolsID)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ToolsID  $toolsID
     * @return \Illuminate\Http\Response
     */
    public function edit(ToolsID $toolsID)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ToolsID  $toolsID
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ToolsID $toolsID)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ToolsID  $toolsID
     * @return \Illuminate\Http\Response
     */
    public function destroy(ToolsID $toolsID)
    {
        //
    }
}
