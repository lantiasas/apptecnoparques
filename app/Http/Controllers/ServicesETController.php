<?php

namespace App\Http\Controllers;

use App\ServicesET;
use Illuminate\Http\Request;

class ServicesETController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $servicesET = \DB::table('ats_services')
        ->join('ats_transversal_services', 'ats_services.id', '=', 'ats_transversal_services.service_id')
        ->join('ats_lines', 'ats_transversal_services.line_id', '=', 'ats_lines.id')
        ->select('ats_services.name','ats_services.id')
        ->where('ats_lines.id', '2')
        ->get();
        return view('servicesET', compact('servicesET'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ServicesET  $servicesET
     * @return \Illuminate\Http\Response
     */
    public function show(ServicesET $servicesET)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ServicesET  $servicesET
     * @return \Illuminate\Http\Response
     */
    public function edit(ServicesET $servicesET)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ServicesET  $servicesET
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ServicesET $servicesET)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ServicesET  $servicesET
     * @return \Illuminate\Http\Response
     */
    public function destroy(ServicesET $servicesET)
    {
        //
    }
}
