<?php

namespace App\Http\Controllers;

use App\Technoparks;
use Illuminate\Http\Request;

class TechnoparksController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $teckPark = \DB::table('ats_technoparks')->select('name','formation_center','address','description','phone')->get();
        return view('tecpark', compact('teckPark')); 
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Technoparks  $technoparks
     * @return \Illuminate\Http\Response
     */
    public function show(Technoparks $technoparks)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Technoparks  $technoparks
     * @return \Illuminate\Http\Response
     */
    public function edit(Technoparks $technoparks)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Technoparks  $technoparks
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Technoparks $technoparks)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Technoparks  $technoparks
     * @return \Illuminate\Http\Response
     */
    public function destroy(Technoparks $technoparks)
    {
        //
    }
}
