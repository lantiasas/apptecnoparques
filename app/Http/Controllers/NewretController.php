<?php

namespace App\Http\Controllers;

use App\newret;
use App\conditions;
use App\restriction;
use App\contacts;
use App\files;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use app\Http\Requests\retrequest;
use App;
use DB;

class NewretController extends Controller
{
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('newret');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $addRet = new newret;
        $addRet ->name = $request ->namert;
        $addRet ->description = $request ->descriptionret;
        $addRet ->scope = $request ->scoperet;
        $addRet ->deadline = $request ->deadlineret;
        $addRet ->approved = '0';
        $addRet ->idusers = Auth::user()->id;
        $addRet->save();

        foreach($request ->conditionsret as $value){
            $conditions = new conditions;
            $conditions->item = $value;
            $conditions->id_challenge = $addRet->id;
            $conditions->save();
        }

        foreach($request ->restrictionret as $value){
            $restrictions = new restriction;
            $restrictions->item = $value;
            $restrictions->id_challenge = $addRet->id;
            $restrictions->save();
        }

        for ($i=0; $i <count($request->nameret); $i++){
            $contact = new contacts;
            $contact->name = $request->nameret[$i];
            $contact->email = $request->emailret[$i];
            $contact->telephone = $request->phoneret[$i];
            $contact->position = $request->positionret[$i];
            $contact->id_challenge  = $addRet->id;
            $contact->save();
        }

        if($request->hasFile('archives')){

            $files = $request->file('archives');
            foreach($files as $file){
                $strfile = new files;
                $nombre = $file->getClientOriginalName();
                //$file->move(public_path().'/archives'.'/'.$addRet->id, $nombre);
                \Storage::disk('chl')->put($addRet->id.'\\'.$nombre,  \File::get($file));
                $strfile->name = $nombre;
                $strfile->id_challenge  = $addRet->id;
                $strfile->save();
            }
        }
        $data = Auth::user()->rol;
        return view('innv', compact('data'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\newret  $newret
     * @return \Illuminate\Http\Response
     */
    public function show(newret $newret)
    {   
        if(Auth::user()->rol == 'administrator') {
            $viewchl = DB::table('users')
            ->join('ats_chl_challenges', 'users.id', '=', 'ats_chl_challenges.idusers')
            ->select('ats_chl_challenges.name as nr', 'ats_chl_challenges.id', 'users.name', 
                     'ats_chl_challenges.description', 'ats_chl_challenges.scope', 
                     'ats_chl_challenges.deadline')->paginate(10);
        }else{
            $viewchl = DB::table('users')
            ->join('ats_chl_challenges', 'users.id', '=', 'ats_chl_challenges.idusers')
            ->select('ats_chl_challenges.name as nr', 'ats_chl_challenges.id', 'users.name', 
                     'ats_chl_challenges.description', 'ats_chl_challenges.scope', 
                     'ats_chl_challenges.deadline')
            ->where('users.id','=',Auth::user()->id)->paginate(10);
        }
        return view('viewchallenge')->with('retos', $viewchl);
    }
    public function view(Request $request)
    {
        $viewchl = DB::table('users')
            ->join('ats_chl_challenges', 'users.id', '=', 'ats_chl_challenges.idusers')
            ->join('ats_chl_restrictions', 'ats_chl_challenges.id', '=', 'ats_chl_restrictions.id_challenge')
            ->select('ats_chl_challenges.name as nr', 'ats_chl_challenges.id','ats_chl_challenges.approved' ,'users.name','users.rol',
                     'ats_chl_challenges.description', 'ats_chl_challenges.scope', 'users.id as idusr', 
                     'ats_chl_challenges.deadline', 'ats_chl_restrictions.item as rest')
            ->where('ats_chl_challenges.id','=',$request->idchl)
            ->get();

        $viewchl1 = DB::table('users')
            ->join('ats_chl_challenges', 'users.id', '=', 'ats_chl_challenges.idusers')

            ->join('ats_chl_conditions', 'ats_chl_challenges.id', '=', 'ats_chl_conditions.id_challenge')
            ->select('ats_chl_conditions.item as cond', 'users.id as idusr')
            ->where('ats_chl_challenges.id','=',$request->idchl)
            ->get();

        $viewchl2 = DB::table('users')
            ->join('ats_chl_challenges', 'users.id', '=', 'ats_chl_challenges.idusers')
            ->join('ats_chl_contacts', 'ats_chl_challenges.id', '=', 'ats_chl_contacts.id_challenge')
            ->select('ats_chl_contacts.name','ats_chl_contacts.email','ats_chl_contacts.telephone','ats_chl_contacts.position', 'users.id as idusr')
            ->where('ats_chl_challenges.id','=',$request->idchl)
            ->get();

        $files = DB::table('ats_chl_files')
            ->select('id','name','id_challenge')
            ->where('id_challenge','=',$request->idchl)
            ->get();

        $data = Auth::user()->rol;
        $idlg = Auth::user()->id;
        //return $files;
        return view('viewselectchl', compact('viewchl','viewchl1','viewchl2','data', 'files','idlg')); //
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\newret  $newret
     * @return \Illuminate\Http\Response
     */
    public function edit(request $newret)
    {
        $chlEdit = newret::findOrfail($newret->idviewchl);
        $chlEdit->approved = '1';
        $chlEdit->save();
        sleep(1);
        $viewchl = DB::table('users')
        ->join('ats_chl_challenges', 'users.id', '=', 'ats_chl_challenges.idusers')
        ->select('ats_chl_challenges.name as nr', 'ats_chl_challenges.id', 'users.name', 
                 'ats_chl_challenges.description', 'ats_chl_challenges.scope', 
                 'ats_chl_challenges.deadline')->paginate(10);
        return view('viewchallenge')->with('retos', $viewchl);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\newret  $newret
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, newret $newret)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\newret  $newret
     * @return \Illuminate\Http\Response
     */
    public function destroy(newret $newret)
    {
        //
    }

    public function chlarchives($volume_id)
    {
        $zip_file = 'challenge.zip';
        $zip = new \ZipArchive();
        $zip->open($zip_file, \ZipArchive::CREATE | \ZipArchive::OVERWRITE);
        
        $path = storage_path('archives/chl/'.$volume_id.'');
        $files = new \RecursiveIteratorIterator(new \RecursiveDirectoryIterator($path));
        foreach ($files as $name => $file)
        {
            if (!$file->isDir()) {
                $filePath = $file->getRealPath();
                $relativePath = substr($filePath, strlen($path) + 1);
        
                $zip->addFile($filePath, $relativePath);
            }
        }
        $zip->close();
        return response()->download($zip_file);
    }
}