<?php

namespace App\Http\Controllers;

use App\SublinesTV;
use Illuminate\Http\Request;

class SublinesTVController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $sublinesTV = \DB::table('ats_sublines')
            ->join('ats_lines', 'ats_sublines.line_id', '=', 'ats_lines.id')
            ->select('ats_sublines.name','ats_sublines.icon')
            ->where('ats_lines.id', '4')
            ->get();
        return view('sublinesTV', compact('sublinesTV'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\SublinesTV  $sublinesTV
     * @return \Illuminate\Http\Response
     */
    public function show(SublinesTV $sublinesTV)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\SublinesTV  $sublinesTV
     * @return \Illuminate\Http\Response
     */
    public function edit(SublinesTV $sublinesTV)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\SublinesTV  $sublinesTV
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, SublinesTV $sublinesTV)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\SublinesTV  $sublinesTV
     * @return \Illuminate\Http\Response
     */
    public function destroy(SublinesTV $sublinesTV)
    {
        //
    }
}
