<?php

namespace App\Http\Controllers;

use App\conditions;
use Illuminate\Http\Request;

class ConditionsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\conditions  $conditions
     * @return \Illuminate\Http\Response
     */
    public function show(conditions $conditions)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\conditions  $conditions
     * @return \Illuminate\Http\Response
     */
    public function edit(conditions $conditions)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\conditions  $conditions
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, conditions $conditions)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\conditions  $conditions
     * @return \Illuminate\Http\Response
     */
    public function destroy(conditions $conditions)
    {
        //
    }
}
