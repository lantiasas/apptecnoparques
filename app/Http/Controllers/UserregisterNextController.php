<?php

namespace App\Http\Controllers;

use App\UserregisterNext;
use Illuminate\Http\Request;

class UserregisterNextController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\UserregisterNext  $userregisterNext
     * @return \Illuminate\Http\Response
     */
    public function show(UserregisterNext $userregisterNext)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\UserregisterNext  $userregisterNext
     * @return \Illuminate\Http\Response
     */
    public function edit(UserregisterNext $userregisterNext)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\UserregisterNext  $userregisterNext
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, UserregisterNext $userregisterNext)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\UserregisterNext  $userregisterNext
     * @return \Illuminate\Http\Response
     */
    public function destroy(UserregisterNext $userregisterNext)
    {
        //
    }
}
