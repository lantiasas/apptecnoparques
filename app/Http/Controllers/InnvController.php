<?php

namespace App\Http\Controllers;

use App\innv;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class InnvController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $data = Auth::user()->rol;
        return view('innv', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\innv  $innv
     * @return \Illuminate\Http\Response
     */
    public function show(innv $innv)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\innv  $innv
     * @return \Illuminate\Http\Response
     */
    public function edit(innv $innv)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\innv  $innv
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, innv $innv)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\innv  $innv
     * @return \Illuminate\Http\Response
     */
    public function destroy(innv $innv)
    {
        //
    }
}
