<?php

namespace App\Http\Controllers;

use App\restriction;
use Illuminate\Http\Request;

class RestrictionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\restriction  $restriction
     * @return \Illuminate\Http\Response
     */
    public function show(restriction $restriction)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\restriction  $restriction
     * @return \Illuminate\Http\Response
     */
    public function edit(restriction $restriction)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\restriction  $restriction
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, restriction $restriction)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\restriction  $restriction
     * @return \Illuminate\Http\Response
     */
    public function destroy(restriction $restriction)
    {
        //
    }
}
