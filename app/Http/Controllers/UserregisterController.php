<?php


namespace App\Http\Controllers;

use App\Userregister;
use Illuminate\Http\Request;
use App;
session_start();
class UserregisterController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function createStep1(Request $request)
    {
        return view('Userregister');
    }

    public function createStep2(Request $request)
    {
        return view('Userregisternext');
    }

    public function createStep3(Request $request)
    {
        return view('Userregisterend');
    }

    public function postCreateStep1(Request $request)
    {
        
        $_SESSION['nameV'] = $request ->name;
        $_SESSION['emailV'] = $request ->email;
        $_SESSION['cityV'] = $request ->city;
        $_SESSION['departamentV'] = $request ->departament;
        return redirect('Userregisternext');

    }
    public function postCreateStep2(Request $request){

        $_SESSION['redParkV'] = $request ->redPark;
        $_SESSION['typemembershipV'] = $request ->typemembership;
        $_SESSION['typecompanyV'] = $request ->typecompany;
        
        return redirect('Userregisterend');

    }
    public function store(Request $request)
    {   
        $addRegister = new Userregister;

        $addRegister ->name = $_SESSION['nameV'];
        $addRegister ->email = $_SESSION['emailV'];
        $addRegister ->city = $_SESSION['cityV'];
        $addRegister ->departament = $_SESSION['departamentV'];

        $addRegister ->redPark = $_SESSION['redParkV'];
        $addRegister ->typemembership = $_SESSION['typemembershipV'];
        $addRegister ->typecompany = $_SESSION['typecompanyV'];

        $addRegister ->sectorcompany = $request ->sectorcompany;
        $addRegister ->help = $request ->help;

        $addRegister->save();
        return redirect('start');
    }
    /**
     * Display the specified resource.
     *
     * @param  \App\Userregister  $userregister
     * @return \Illuminate\Http\Response
     */
    public function show(Userregister $userregister)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Userregister  $userregister
     * @return \Illuminate\Http\Response
     */
    public function edit(Userregister $userregister)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Userregister  $userregister
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Userregister $userregister)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Userregister  $userregister
     * @return \Illuminate\Http\Response
     */
    public function destroy(Userregister $userregister)
    {
        //
    }
}
