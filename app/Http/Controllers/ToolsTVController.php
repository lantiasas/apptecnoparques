<?php

namespace App\Http\Controllers;

use App\ToolsTV;
use Illuminate\Http\Request;

class ToolsTVController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $InfrastructureTV = \DB::table('ats_tools')
        ->join('ats_lines', 'ats_tools.line_id', '=', 'ats_lines.id')
        ->select('ats_tools.name')
        ->where('ats_lines.id', '4')
        ->get();
        return view('InfrastructureTV', compact('InfrastructureTV'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ToolsTV  $toolsTV
     * @return \Illuminate\Http\Response
     */
    public function show(ToolsTV $toolsTV)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ToolsTV  $toolsTV
     * @return \Illuminate\Http\Response
     */
    public function edit(ToolsTV $toolsTV)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ToolsTV  $toolsTV
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ToolsTV $toolsTV)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ToolsTV  $toolsTV
     * @return \Illuminate\Http\Response
     */
    public function destroy(ToolsTV $toolsTV)
    {
        //
    }
}
