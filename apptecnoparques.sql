-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 02-09-2020 a las 00:31:17
-- Versión del servidor: 10.4.11-MariaDB
-- Versión de PHP: 7.2.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `apptecnoparques`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ats_chl_challenges`
--

CREATE TABLE `ats_chl_challenges` (
  `id` int(11) NOT NULL,
  `name` varchar(100) CHARACTER SET utf8 COLLATE utf8_spanish_ci DEFAULT NULL,
  `description` text CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `scope` text CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `deadline` varchar(50) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `approved` tinyint(4) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `idusers` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `ats_chl_challenges`
--

INSERT INTO `ats_chl_challenges` (`id`, `name`, `description`, `scope`, `deadline`, `approved`, `created_at`, `idusers`) VALUES
(1, 'sdfsdfsdfs', 'fsfsdfsd', 'fsdfds', 'fdsfsd', 0, '2020-08-04 20:48:05', 2),
(2, 'dsafsdfsd', 'sdfsdfds', 'sdfdsfds', 'sdfdsf', 1, '2020-08-04 21:03:40', 2),
(3, NULL, 'sdfsfsdxxxx', 'sdfsdxxxxxf', 'xxxx', 0, '2020-08-21 01:31:46', 3);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ats_chl_conditions`
--

CREATE TABLE `ats_chl_conditions` (
  `id` int(11) NOT NULL,
  `item` text CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `id_challenge` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `ats_chl_conditions`
--

INSERT INTO `ats_chl_conditions` (`id`, `item`, `id_challenge`) VALUES
(1, 'sdfsdf', 1),
(2, 'sdfsdf', 2),
(3, 'xxx', 3),
(4, 'xxx', 3);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ats_chl_contacts`
--

CREATE TABLE `ats_chl_contacts` (
  `id` int(11) NOT NULL,
  `name` varchar(100) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `email` varchar(100) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `telephone` bigint(20) NOT NULL,
  `position` varchar(45) CHARACTER SET utf8 COLLATE utf8_spanish_ci DEFAULT NULL,
  `id_challenge` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `ats_chl_contacts`
--

INSERT INTO `ats_chl_contacts` (`id`, `name`, `email`, `telephone`, `position`, `id_challenge`) VALUES
(1, 'dfgdfg', 'dfgdfgd@fsdgmail.com', 236547888, NULL, 1),
(2, 'sdfsdfdsfds', 'sdsdf@dfsff.fff', 236547888, NULL, 2),
(3, 'xxxx', 'xxx@sss.xxx', 6565656565656, 'xxxxxxxxxxx', 3),
(4, 'cccc', 'cccc@ccc.ccc', 31231256464, 'ccccccc', 3);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ats_chl_files`
--

CREATE TABLE `ats_chl_files` (
  `id` int(11) NOT NULL,
  `name` varchar(100) CHARACTER SET utf8 COLLATE utf8_spanish_ci DEFAULT NULL,
  `id_challenge` int(11) DEFAULT NULL,
  `id_solution` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `ats_chl_files`
--

INSERT INTO `ats_chl_files` (`id`, `name`, `id_challenge`, `id_solution`) VALUES
(2, '[000009].jpg', 3, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ats_chl_restrictions`
--

CREATE TABLE `ats_chl_restrictions` (
  `id` int(11) NOT NULL,
  `item` text CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `id_challenge` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `ats_chl_restrictions`
--

INSERT INTO `ats_chl_restrictions` (`id`, `item`, `id_challenge`) VALUES
(1, 'sdfsdf', 1),
(2, 'sdfsdfs', 2),
(3, 'xxx', 3);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ats_chl_solutions`
--

CREATE TABLE `ats_chl_solutions` (
  `id` int(11) NOT NULL,
  `description` text CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `approved` tinyint(4) NOT NULL,
  `id_challenge` int(11) NOT NULL,
  `idusers` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `ats_chl_solutions`
--

INSERT INTO `ats_chl_solutions` (`id`, `description`, `approved`, `id_challenge`, `idusers`) VALUES
(1, 'sdjkhfghsdjgfhsdjgfhjsdgfjhdsgfhjdsgfjdgdsjhjfdhsgfdshghdgfhdjghjsgfjsghsghjsghjghdgfhdgfhjdghsgf', 0, 2, 3),
(2, 'kjdshfdsjhfkjdshfkjdshfkjsdfsdfjshdgfhdsjgfdhsjfghdsjgfhdjsgf', 0, 2, 3);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ats_lines`
--

CREATE TABLE `ats_lines` (
  `id` int(11) NOT NULL,
  `name` varchar(45) COLLATE utf8mb4_spanish_ci NOT NULL,
  `description` longtext COLLATE utf8mb4_spanish_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish_ci;

--
-- Volcado de datos para la tabla `ats_lines`
--

INSERT INTO `ats_lines` (`id`, `name`, `description`) VALUES
(1, 'Biotecnología y NanoTecnología', 'Las aplicaciones biotecnológicas hacen uso de\r\nlos sistemas biológicos y organismos vivos o sus\r\nderivados para la creación o modificación de\r\nproductos o procesos. La Nanotecnología\r\nbusca el desarrollo de sistemas en escala\r\nnanométrica para usos específicos.'),
(2, 'Electrónica y Telecomunicaciones', 'Es la serie de conocimientos experimentados, en el que se adquiere una serie de productos tecnológicos para aplicar en las distintas áreas de las telecomunicaciones (televisión, telefonía móvil, robótica, técnicas de sistematización en las industrias, dispositivos biomédicos, mecanismos de alarma y monitoreo)'),
(3, 'Ingeniería y Diseño', 'Es el área cuyo objetivo es crear y recrear un objeto para su óptima producción, distribución, comercialización y uso. A todo este proceso se anexan tecnologías, que colaboran a una mejor utilización de materiales.'),
(4, 'Tecnologías Virtuales\r\n', 'Son las tecnologías digitales en el diseño gráfico usadas para el e-learning, desarrollos de software, multimedia, aplicaciones para móviles y web, gestión de información (bases de datos), modelado y animación 3d, creación de mundos virtuales (2.5d).');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ats_lines_in_techoparks`
--

CREATE TABLE `ats_lines_in_techoparks` (
  `id` int(11) NOT NULL,
  `line_id` int(11) NOT NULL,
  `technopark_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish_ci;

--
-- Volcado de datos para la tabla `ats_lines_in_techoparks`
--

INSERT INTO `ats_lines_in_techoparks` (`id`, `line_id`, `technopark_id`) VALUES
(1, 1, 1),
(2, 3, 1),
(3, 1, 2),
(4, 2, 2),
(5, 3, 2),
(6, 4, 2),
(7, 1, 3),
(8, 2, 3),
(9, 3, 3),
(10, 4, 3),
(11, 1, 4),
(12, 2, 4),
(13, 3, 4),
(14, 4, 4),
(15, 2, 5),
(16, 3, 5),
(17, 4, 5),
(18, 1, 6),
(19, 2, 6),
(20, 3, 6),
(21, 4, 6),
(22, 1, 7),
(23, 2, 7),
(24, 3, 7),
(25, 4, 7),
(26, 1, 8),
(27, 4, 8),
(28, 2, 9),
(29, 3, 9),
(30, 4, 9),
(31, 2, 10),
(32, 3, 10),
(33, 4, 10),
(34, 1, 11),
(35, 2, 11),
(36, 3, 11),
(37, 4, 11),
(38, 1, 12),
(39, 4, 12),
(40, 1, 13),
(41, 2, 13),
(42, 3, 13),
(43, 4, 13),
(44, 1, 14),
(45, 2, 14),
(46, 3, 14),
(47, 4, 14),
(48, 1, 15),
(49, 2, 15),
(50, 3, 15),
(51, 1, 16),
(52, 2, 16),
(53, 3, 16),
(54, 4, 16);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ats_projects`
--

CREATE TABLE `ats_projects` (
  `id` int(11) NOT NULL,
  `name` varchar(45) COLLATE utf8mb4_spanish_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_spanish_ci DEFAULT NULL,
  `description` longtext COLLATE utf8mb4_spanish_ci DEFAULT NULL,
  `line_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ats_services`
--

CREATE TABLE `ats_services` (
  `id` int(11) NOT NULL,
  `name` varchar(100) COLLATE utf8mb4_spanish_ci NOT NULL,
  `image` varchar(45) COLLATE utf8mb4_spanish_ci DEFAULT NULL,
  `description` longtext COLLATE utf8mb4_spanish_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish_ci;

--
-- Volcado de datos para la tabla `ats_services`
--

INSERT INTO `ats_services` (`id`, `name`, `image`, `description`) VALUES
(1, 'Biotecnología Agroindustrial', NULL, NULL),
(2, 'Aprovechamiento de residuos agroIndustriales', NULL, NULL),
(3, 'Tecnología en procesos de Alimentos', NULL, NULL),
(4, 'Producción de alimentos para animales', NULL, NULL),
(5, 'Biotecnología Industrial', NULL, NULL),
(6, 'Biofarmaceutica', NULL, NULL),
(7, 'Anfibióticos', NULL, NULL),
(8, 'Biocompbustibles', NULL, NULL),
(9, 'Biotecnología Animal', NULL, NULL),
(10, 'Diseño de planta para productos biotecnológicos', NULL, NULL),
(11, 'Análisis de análisis de calidad de la leche', NULL, NULL),
(12, 'Coreotipo bovino, caprino y ovino', NULL, NULL),
(13, 'Evaluación hematológica y de química sanguínea', NULL, NULL),
(14, 'Congelación de semen y embriones', NULL, NULL),
(15, 'Nutrición animal', NULL, NULL),
(16, 'Mejoramiento genético', NULL, NULL),
(17, 'Biotecnología Vegetal', NULL, NULL),
(18, 'Cultivo de Tejidos', NULL, NULL),
(19, 'Propagación vegetal', NULL, NULL),
(20, 'Controladores biológicos', NULL, NULL),
(21, 'Bioinsumos', NULL, NULL),
(22, 'Biotecnología Animal', NULL, NULL),
(23, 'Biorremediación', NULL, NULL),
(24, 'Análisis de agua', NULL, NULL),
(25, 'Mejoramiento de suelos', NULL, NULL),
(26, 'Automatización e Instrumentación', NULL, NULL),
(27, 'Diseño de soluciones industriales', NULL, NULL),
(28, 'Instrumentación y monitoreo de procesos', NULL, NULL),
(29, 'Desarrollo de técnicas de control para procesos', NULL, NULL),
(30, 'Optimización de procesos productivos', NULL, NULL),
(31, 'Diseño y Fabricación de Circuitos PCB', NULL, NULL),
(32, 'Desarrollo de prototipos electrónicos', NULL, NULL),
(33, 'Pruebas y puesta en marcha de prototipos', NULL, NULL),
(34, 'Sistemas de Energías Renovables', NULL, NULL),
(35, 'Diseño de soluciones de optimización de energía', NULL, NULL),
(36, 'Inspección de sistemas eléctricos', NULL, NULL),
(37, 'Pruebas t ouesta en marcha de sistemas solares', NULL, NULL),
(38, 'Microcontroladores ARM', NULL, NULL),
(39, 'Programación de plataformas ARM', NULL, NULL),
(40, 'Programación de plataformas embebidas', NULL, NULL),
(41, 'Desarrollo de aplicaciones', NULL, NULL),
(42, 'Diseño y Simulación', NULL, NULL),
(43, 'Modelado de prototipos 3D', NULL, NULL),
(44, 'Análisis, simulación, validación estructural ', NULL, NULL),
(45, 'Anímación y renderización fotorealista avanzada', NULL, NULL),
(46, 'Robótica', NULL, NULL),
(47, 'Diseño de moldes y creación de matrices', NULL, NULL),
(48, 'Prototipado', NULL, NULL),
(49, 'Prototipado manual', NULL, NULL),
(50, 'Prototipados CAD/CAM 2D y 3D', NULL, NULL),
(51, 'Ingeniería Inversa', NULL, NULL),
(52, 'Análisis de ciclo de vida de productos', NULL, NULL),
(53, 'Validación e inspección dimensional y funcional', NULL, NULL),
(54, 'Reproducción, reconstrucción y/o modelado de piezas', NULL, NULL),
(55, 'Validación de siseños de piezas y ensamblajes 2D Y 3D', NULL, NULL),
(56, 'Branding - Marketing Digital', NULL, NULL),
(57, 'Marca', NULL, NULL),
(58, 'Producto', NULL, NULL),
(59, 'Desarrollo de Sistemas Informáticos ', NULL, NULL),
(60, 'Plataformas web', NULL, NULL),
(61, 'Plataformas Moviles', NULL, NULL),
(62, 'Plataformas Clientes/Servidores', NULL, NULL),
(63, 'Producción de contenidos 2D y 3D', NULL, NULL),
(64, 'Realidad Virtual y Aumentada', NULL, NULL),
(65, 'Plataformas Standalione', NULL, NULL),
(66, 'Desarrrollo y Diseño de Videojuegos', NULL, NULL),
(67, 'Captura de movimiento', NULL, NULL),
(68, 'Desarrollo de video juego', NULL, NULL),
(69, 'Animación Digital', NULL, NULL),
(70, 'Inteligencia Artificial y Ciencias Computacionales', NULL, NULL),
(71, 'Deep Learning', NULL, NULL),
(72, 'Machine Learning', NULL, NULL),
(73, 'Internet de las cosas IoT', NULL, NULL),
(74, 'Big Data', NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ats_sublines`
--

CREATE TABLE `ats_sublines` (
  `id` int(11) NOT NULL,
  `name` varchar(100) COLLATE utf8mb4_spanish_ci NOT NULL,
  `icon` varchar(45) COLLATE utf8mb4_spanish_ci DEFAULT NULL,
  `description` longtext COLLATE utf8mb4_spanish_ci DEFAULT NULL,
  `line_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish_ci;

--
-- Volcado de datos para la tabla `ats_sublines`
--

INSERT INTO `ats_sublines` (`id`, `name`, `icon`, `description`, `line_id`) VALUES
(1, 'Biotecnología Industrial', NULL, NULL, 1),
(2, 'Microbiología agrícola y pecuaria', NULL, NULL, 1),
(3, 'Biotecnología Animal', NULL, NULL, 1),
(4, 'Biotecnología Vegetal', NULL, NULL, 1),
(5, 'Bioinformática', NULL, NULL, 1),
(6, 'Medio ambiente', NULL, NULL, 1),
(7, 'Nuevos materiales', NULL, NULL, 1),
(8, 'Energías verdes y biocombustibles', NULL, NULL, 1),
(9, 'Agroindustria alimentaria', NULL, NULL, 1),
(10, 'Agroindustria no alimentaria', NULL, NULL, 1),
(11, 'Nanotecnología', NULL, NULL, 1),
(12, 'Automatización e instrumentación', NULL, NULL, 2),
(13, 'Redes inteligente', NULL, NULL, 2),
(14, 'Sistemas embebidos', NULL, NULL, 2),
(15, 'Agroelectrónica', NULL, NULL, 2),
(16, 'Análisis de señales y protocolos', NULL, NULL, 2),
(17, 'Infraestructura, redes y antenas', NULL, NULL, 2),
(18, 'Diseño electrónico', NULL, NULL, 2),
(19, 'Telemática', NULL, NULL, 2),
(20, 'Internet de las cosas (IoT)', NULL, NULL, 2),
(21, 'Robótica', NULL, NULL, 2),
(22, 'Productos y procesos', NULL, NULL, 3),
(23, 'Diseño de concepto y detalles', NULL, NULL, 3),
(24, 'Análisis y simulación', NULL, NULL, 3),
(25, 'Ingeniería inversa', NULL, NULL, 3),
(26, 'Mecanizado', NULL, NULL, 3),
(27, 'Diseño Estratégico', NULL, NULL, 3),
(28, 'Biomecánica', NULL, NULL, 3),
(29, 'Materiales', NULL, NULL, 3),
(30, 'Tecnificación de procesos agrícolas', NULL, NULL, 3),
(31, 'Aplicación de energías renovables', NULL, NULL, 3),
(32, 'Sistemas el aprovechamiento de recursos hídricos', NULL, NULL, 3),
(33, 'Aplicaciones Móviles', 'TV--AplicacionesMoviles.png', NULL, 4),
(34, 'Inteligencia Artificial', 'TV--InteligenciaArtificial.png', NULL, 4),
(35, 'Big-Data', 'TV--BigData.png', NULL, 4),
(36, 'Realidad Aumentada', 'TV--RealidadAumentada.png', NULL, 4),
(37, 'Animación Digital', 'TV--AnimacionDigital.png', NULL, 4),
(38, 'Diseño y Desarrollo de Videojuegos', 'TV--DiseñoDlloVideojuegos.png', NULL, 4),
(39, 'Ingeniería de software', 'TV--IngenieriaSoftware.png', NULL, 4),
(40, 'Desarrollo de contenidos multimediales', 'masicons-14.png', NULL, 4),
(41, 'Geotecnología', 'masicons-15.png', NULL, 4),
(42, 'Realidad Virtual y Simulación', 'masicons-16.png', NULL, 4);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ats_technoparks`
--

CREATE TABLE `ats_technoparks` (
  `id` int(11) NOT NULL,
  `name` varchar(45) COLLATE utf8mb4_spanish_ci NOT NULL,
  `description` longtext COLLATE utf8mb4_spanish_ci DEFAULT NULL,
  `region` varchar(200) COLLATE utf8mb4_spanish_ci DEFAULT NULL,
  `formation_center` varchar(200) COLLATE utf8mb4_spanish_ci DEFAULT NULL,
  `address` varchar(50) COLLATE utf8mb4_spanish_ci DEFAULT NULL,
  `link` varchar(255) COLLATE utf8mb4_spanish_ci DEFAULT NULL,
  `phone` varchar(45) COLLATE utf8mb4_spanish_ci DEFAULT NULL,
  `latitude` double NOT NULL,
  `longitude` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish_ci;

--
-- Volcado de datos para la tabla `ats_technoparks`
--

INSERT INTO `ats_technoparks` (`id`, `name`, `description`, `region`, `formation_center`, `address`, `link`, `phone`, `latitude`, `longitude`) VALUES
(1, 'Angostura', 'El Tecnoparque Angostura dedicada a la investigación aplicada, desarrollo y transferencia de tecnología al entorno rural. La misión del Tecnoparque Angostura es fortalecer empresas y emprendimientos del campo, creando nuevos productos y prototipos. El Tecnoparque cuenta con ambientes especializados de tecnologías virtuales, electrónica y Telecomunicaciones e Ingeniería y Diseño.', 'Centro Sur Amazonía\r\n', 'CENTRO DE FORMACIÓN AGROINDUSTRIAL\r\n', 'Km38 al sur de Neiva, municipio de Campoalegre', 'https://www.google.com/maps/place/Centro+de+Formación+Agroindustrial+La+Angostura/@2.6125611,-75.3636236,17z/data=!3m1!4b1!4m5!3m4!1s0x8e3b3f4b1c54ddc5:0x6a0d5a458d5d190d!8m2!3d2.6125611!4d-75.3614349', '8380191 Ext 83570', 2.614176, -75.36115),
(2, 'Bucaramanga', 'El Tecnoparque Bucaramanga es un acelerador de ideas de base tecnológica. Su actuación está relacionada con los TRL (Technology Readiness Level) referido a la madurez de los proyectos presentados al Tecnoparque, con un enfoque en prototipos y en productos con TRL 6 en adelante. El Tecnoparque cuenta con un lugar abierto, el cual se adecua y tiene las siguientes áreas cerradas: Laboratorio especializado de bIotecnología y nanotecnología; máquinas y herramientas; circuitos, captura de movimiento. También, tienen un espacio de coworking.', 'Centro Oriente\r\n', 'CENTRO INDUSTRIAL DEL DISEÑO Y LA MANUFACTURA\r\n', 'Cra. 23 #39-38, Bucaramanga, Santander', 'https://wego.here.com/directions/mix//Tecnoparque-Nodo-Bucaramanga,-Caldas-Reposo,-Bucaramanga,-680003,-Colombia:e-eyJuYW1lIjoiVGVjbm9wYXJxdWUgTm9kbyBCdWNhcmFtYW5nYSIsImFkZHJlc3MiOiJDYXJyZXJhIDIzICMzOS0zOC4gIEJ1Y2FyYW1hbmdhIC0gU2FudGFuZGVyLCBCdWNhcmFtYW5n', 'Teléfonos: 6800600 Ext: 74518\n', 7.100722, -73.106375),
(3, 'Bogotá', 'El Tecnoparque Bogotá es un actor de ciencia tecnología e innovación, y un brazo de investigación aplicada del SENA para atender al sector productivo. El Tecnoparque cuenta con ambiente especializado de tecnologías virtuales, electrónica y telecomunicaciones, ingeniería y diseño; y biotecnología y nanotecnología. ', 'Centro Oriente\r\n', 'CENTRO METALMECÁNICO\r\n', 'Cl. 54 #10-39, Bogotá', 'https://www.google.com/maps/search/Tecnoparque+Bogot%C3%A1/@4.6414694,-74.0643845,14.5z', '5461500 ext 16802\n', 4.640411, -74.065021),
(4, 'Cali', 'La misión del Tecnoparque se enfatiza en apoyar las ideas del público (empresas, personas y universidades) por medio de la conversión de ideas en productos reales. El Tecnoparque cuenta con ambiente especializado en: tecnologías virtuales, electrónica y telecomunicaciones, ingeniería y diseño y biotecnología y nanotecnología ', 'Pacifíco\r\n', 'NATIONAL CENTER FOR TECHNICAL ASSISTANCE TO INDUSTRY - ASTN\r\n', 'Carrera 5 # 11 - 68, COMUNA 3, CENTRO DE CALI', 'https://www.google.com/maps/place/Tecnoparque+Sena/@3.4514509,-76.5322433,15z/data=!4m2!3m1!1s0x0:0x91ec224dfd550379?sa=X&ved=2ahUKEwjKn4qlwfXpAhUMd98KHVBIDjIQ_BIwEXoECA4QCA', '3166227302', 3.451642, -76.532252),
(5, 'Cazúca', 'El Tecnoparque Cazucá plantea su misión como una estrategia para acelerar la I+D+i por medio del desarrollo de prototipos. El Tecnoparque cuenta con: ambiente especializado de tecnologías virtuales, electrónica y Telecomunicaciones, ingeniería y diseño.', 'Centro Oriente\r\n', 'CAZUCA- CENTRO INDUSTRIAL Y DE DESARROLLO EMPRESARIA\r\n', '40 #11-239 a 11-3', 'https://www.google.com/maps/place/Quintanares,+Soacha,+Cundinamarca/@4.5919225,-74.1919596,18z/data=!3m1!4b1!4m12!1m6!3m5!1s0x0:0x8f5115bb30158afc!2sTecnoParque+SENA+Cazuc%C3%A1!8m2!3d4.59263!4d-74.19126!3m4!1s0x8e3f9e4b07b239bb:0x5aa65d7bce266456!8m2!3d4', '5461600 Ext 16802', 4.591963, -74.190932),
(6, 'Manizales', 'La misión del Tecnoparque Manizales asesora y acelera proyectos de investigación, desarrollo tecnológico e innovación, enfocado en el apoyo a empresarios, emprendedores y grupos investigación. El Tecnoparque cuenta con ambientes especializados de biotecnología y nanotecnología, tecnologías virtuales, electrónica y telecomunicaciones.', 'Eje Cafetero\r\n', 'CENTRO DE PROCESOS INDUSTRIALES Y LA CONSTRUCCIÓN\r\n', 'Km 10 Via Magdalena, Manizales, Caldas', 'https://www.google.com/maps/place/TECNOPARQUE+SENA+NODO+MANIZALES/@5.0331201,-75.4502224,15z/data=!4m5!3m4!1s0x0:0x5f219cd95fae0516!8m2!3d5.0331201!4d-75.4502224', '8748444', 5.033996, -75.450373),
(7, 'Medellín', 'La misión del Tecnoparque es ser acelerador de proyectos potencialmente innovadores o de productos mínimos viables. El Tecnoparque cuenta con ambiente especializado de: biotecnología y nanotecnología, tecnologías virtuales, electrónica y Telecomunicaciones, ingeniería y diseño.', 'Eje Cafetero\r\n', 'CENTRO DE SERVICIOS Y GESTIÓN EMPRESARIAL\r\n', 'N° 56 Piso 6 y, Cra. 46 #10 Sur11, Medellín, ', 'https://www.google.com/maps/place/Tecnoparque+SENA+nodo+Medell%C3%ADn/@6.2528773,-75.5628146,15z/data=!4m5!3m4!1s0x0:0xf5a58873988a10eb!8m2!3d6.2528773!4d-75.5628146', '320 418 55 52', 6.252893, -75.562831),
(8, 'La Granja', 'El Tecnoparque La Granja tiene la misión de apoyar a todos los emprendedores y empresarios para generar impacto en la Investigación, Desarrollo e Innovación. El Tecnoparque cuenta con ambientes especializados de tecnologías virtuales; laboratorios especializados de biotecnología y nanotecnología. ', 'Centro Sur Amazonía\r\n', 'CENTER OF INDUSTRY, BUSINESS AND SERVICES\r\n', 'Diagonal 20 # 38 - 16', 'https://www.google.com/maps/place/TecnoParque+Nodo+la+Granja/@4.1755377,-74.9302445,14z/data=!4m5!3m4!1s0x0:0xb2b2b3e020d0ff7f!8m2!3d4.172349!4d-74.9301372', '8670078 Ext 85855', 4.173184, -74.930459),
(9, 'Neiva', 'El Tecnoparque Neiva es una entidad sin ánimo de lucro dedicada a la investigación aplicada, desarrollo y transferencia de tecnología al entorno productivo. La misión del Tecnoparque Neiva está relacionada con la mejora y la productividad de las empresas a través del desarrollo de proyectos de base tecnológica para la mejora de los procesos y servicios de las mismas, generando bienestar en la comunidad. El Tecnoparque cuenta con ambiente especializado de: tecnologías virtuales, electrónica y Telecomunicaciones, ingeniería y diseño y otros.', 'Centro Sur Amazonía\r\n', 'CENTRO DE LA INDUSTRIA, LA EMPRESA Y LOS SERVICIOS\r\n', 'Diagonal 20 # 38 – 16', 'https://www.google.com/maps/place/Tecnoparque,+Av.+Buganviles,+Neiva,+Huila/@2.9408912,-75.2644278,17z/data=!3m1!4b1!4m5!3m4!1s0x8e3b743cf9de584f:0xdc7be2f9bc77f92c!8m2!3d2.9408982!4d-75.2622619', '8365960 Ext 83908', 2.941047, -75.262266),
(10, 'Ocaña', 'El Tecnoparque Ocaña es una entidad sin ánimo de lucro dedicada a la investigación aplicada, desarrollo y transferencia de tecnología al entorno rural. La misión del Tecnoparque Ocaña es acompañar a emprendedores y empresas en la creación de su proyecto de vida desde el autoempleo o la construcción de su empresa.', 'Centro Oriente\r\n', 'CENTRO DE LA INDUSTRIA, LA EMPRESA Y LOS SERVICIOS\r\n', 'Transversal 30 # 7 -110 Barrio la primavera', 'https://www.google.com/maps/place/Tecnoparque+Colombia+Nodo+Oca%C3%B1a+-+SENA/@8.2620158,-73.3447462,12.96z/data=!4m12!1m6!3m5!1s0x0:0xd29509ced014fdd0!2sTecnoparque+Colombia+Nodo+Oca%C3%B1a+-+SENA!8m2!3d8.2574122!4d-73.3588334!3m4!1s0x0:0xd29509ced014fdd', '5611035', 8.258428, -73.358982),
(11, 'Pereira', 'El Tecnoparque Pereira es una entidad sin ánimo de lucro dedicada a la investigación aplicada, desarrollo y transferencia de tecnología al entorno productivo. La misión del Tecnoparque Pereira es acompañar al público en general (universitarios, empresarios, emprendedores, etc.) en el desarrollo de un prototipo funcional. El Tecnoparque cuenta con ambiente especializado de tecnologías virtuales; ambiente especializado de electrónica y telecomunicaciones; ambiente especializado de ingeniería y diseño; y laboratorio especializado de biotecnología y nanotecnología.', 'Eje Cafetero\r\n', 'CENTRO ATENCIÓN AL SECTOR AGROPECUARIO\r\n', 'Carrera 8 No. 26-79', 'https://www.google.com/maps/place/Tecnoparque+SENA+-+Nodo+Pereira/@4.814132,-75.7006346,15z/data=!4m5!3m4!1s0x0:0xd5dbbc9ef6f29d!8m2!3d4.814132!4d-75.7006346', '3135800 IP 63122', 4.814838, -75.700613),
(12, 'Pitalito', 'El Tecnoparque Pitalito es una entidad sin ánimo de lucro dedicada a la investigación aplicada, desarrollo y transferencia de tecnología al entorno rural. La misión del Tecnoparque es apoyar empresarios, aprendices y emprendedores en el desarrollo de proyectos de base tecnológica en un corto tiempo, así, como apoyar la apropiación de esas tecnologías en el sector productivo. El Tecnoparque cuenta con	ambiente especializado de: tecnologías virtuales, diseño y elaboración de prototipos, espacio para asesorías técnicas especializadas y laboratorio especializado de biotecnología y nanotecnología', 'Centro Sur Amazonía\r\n', 'CENTRO DE GESTIÓN Y DESARROLLO SOSTENIBLE SURCOLOMBIANO\r\n', 'Km 7 vereda aguadas', 'https://www.google.com/maps/place/Tecnoparque+7+Agroecol%C3%B3gico+Yamboro/@1.8921904,-76.0903752,15z/data=!4m12!1m6!3m5!1s0x0:0x10e75cc78a354f15!2sTecnoparque+7+Agroecol%C3%B3gico+Yamboro!8m2!3d1.8921904!4d-76.0903752!3m4!1s0x0:0x10e75cc78a354f15!8m2!3d1', '8365960 Ext 83908', 1.892255, -76.09044),
(13, 'Popayán', 'Es un programa de innovación tecnológica del SENA, dirigido a todos los colombianos interesados en desarrollar proyectos de Investigación, Desarrollo e innovación (I+D+i) materializados en prototipos funcionales que promueven el emprendimiento de base tecnológica.', 'Pacifíco\r\n', 'Centro de comercio y servicios Popayán\r\n', 'Carrera 3 # 3-01', NULL, '3197621468', 0, 0),
(14, 'Rionegro', 'El Tecnoparque es un actor que reconoce las necesidades del sector empresarial e identifica  emprendedores apasionados por la innovación y con conocimientos. El Tecnoparque cuenta con ambiente especializado de tecnologías virtuales, electrónica y telecomunicaciones, ingeniería y diseño, etc. ', 'Eje Cafetero\r\n', 'CENTRO DE LA INNOVACIÓN, LA AGROINDUSTRIA Y LA AVIACIÓN.\r\n', 'Zona Franca de rionegro, vereda chachafruto b', 'https://www.google.com/maps/place/TecnoParque+Nodo+Rionegro/@6.1561597,-75.4156538,15z/data=!4m5!3m4!1s0x0:0xc299568dcf141f84!8m2!3d6.1561597!4d-75.4156538', '5311856 Ext 44052', 6.1561597, -75.4156538),
(15, 'Socorro', 'El Tecnoparque Socorro es una entidad sin ánimo de lucro dedicada a la investigación aplicada, desarrollo y transferencia de tecnología al entorno rural. La misión del Tecnoparque es apoyar a los empresarios, talentos y a todas las personas que tengan una idea para mejorar un proceso o crear un prototipo. El Tecnoparque cuenta con ambiente especializado de tecnologías virtuales, electrónica y telecomunicaciones, ingeniería y diseño, etc.', 'Centro Oriente\r\n', 'CENTRO AGROTURISTICO SAN GIL\r\n', 'Socorrro Calle 16 # 14 -28, Centro - Felix A.', 'https://www.google.com/maps/place/Tecnoparque+Nodo+SOCORRO/@6.4711446,-73.2610732,15z/data=!4m2!3m1!1s0x0:0xd6755136b807ecce?sa=X&ved=2ahUKEwivjanX2vXpAhUlTt8KHdazA7kQ_BIwFnoECA8QCA', '7296851', 6.4711446, -73.2610732),
(16, 'Valledupar', 'El Tecnoparque Valledupar es una entidad sin ánimo de lucro dedicada a la investigación aplicada, desarrollo y transferencia de tecnología al entorno productivo. La misión del Tecnoparque Valledupar es apoyar a la comunidad en general en el desarrollo de prototipos funcionales. El Tecnoparque cuenta con ambientes especializado de tecnologías virtuales, ambientes especializados en electrónica y Telecomunicaciones, ambientes especializados en ingeniería y diseño, etc. ', 'Caribe\r\n', 'CENTRO DE OPERACIÓN Y MANTENIMIENTO MINERO\r\n', 'Cr 5 # 14 - 81', 'https://www.google.com/maps/place/Red+De+Tecnoparques+Sena/@10.4779254,-73.2448072,19.75z/data=!4m5!3m4!1s0x8e8ab9b3ae182d1d:0x5400161f23447ac7!8m2!3d10.4783751!4d-73.2445494?hl=en-US', '5842630', 10.4779254, -73.2448072);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ats_tools`
--

CREATE TABLE `ats_tools` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_spanish_ci DEFAULT NULL,
  `description` longtext COLLATE utf8mb4_spanish_ci DEFAULT NULL,
  `line_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish_ci;

--
-- Volcado de datos para la tabla `ats_tools`
--

INSERT INTO `ats_tools` (`id`, `name`, `description`, `line_id`) VALUES
(1, 'Microscopio electónico de barrido (SEM)', NULL, 1),
(2, 'Espectroscopio de absorción atómica', NULL, 1),
(3, 'Rotaevaporador', NULL, 1),
(4, 'Centrifuga', NULL, 1),
(5, 'Extractor de Grasas', NULL, 1),
(6, 'Análizador de fibras', NULL, 1),
(7, 'Estación meteorológica', NULL, 1),
(8, 'Equipo de hematología', NULL, 1),
(9, 'Equipo de química sanguínea', NULL, 1),
(10, 'Microscopio', NULL, 1),
(11, 'Estereoscopio', NULL, 1),
(12, 'Espectrofotómetro', NULL, 1),
(13, 'Autoclave', NULL, 1),
(14, 'Medidor de humedad', NULL, 1),
(15, 'Mufla', NULL, 1),
(16, 'Horno de convección', NULL, 1),
(17, 'Multiparámetros', NULL, 1),
(18, 'pHmetro', NULL, 1),
(19, 'Ecógrafo', NULL, 1),
(20, 'Electro-eyaculador', NULL, 1),
(21, 'Kit de inseminación artificial laparoscópica', NULL, 1),
(22, 'Equipo de congelación de semen y embriones', NULL, 1),
(23, 'Prototipadora para fabricación de circuitos impresos', NULL, 2),
(24, 'Impresora manual de sobremesa para pasta de soldadura sobre PCB', NULL, 2),
(25, 'Horno de reflujo para soldadura blanda', NULL, 2),
(26, 'Estación de soldadura', NULL, 2),
(27, 'Módulo simulación de energías', NULL, 2),
(28, 'PLC\'s (Controladores Logísticos Programables)', NULL, 2),
(29, 'Tarjetas de adquisición de datos', NULL, 2),
(30, 'Osciloscopios', NULL, 2),
(31, 'Multímetros', NULL, 2),
(32, 'Kit de robótica', NULL, 2),
(33, 'FPGA (Field Programmable Gate Away)', NULL, 2),
(34, 'Software de desarrollo', NULL, 2),
(35, 'Bancos de trabajo para electrónica', NULL, 2),
(36, 'Impesoras 3D por ABS, fibra de cambono y estereolitografía', NULL, 3),
(37, 'Escaner 3D', NULL, 3),
(38, 'Ruteadora CNC', NULL, 3),
(39, 'Termoformadora', NULL, 3),
(40, 'Cortadora laser', NULL, 3),
(41, 'Drone', NULL, 3),
(42, 'Equipo de soldadura eléctrica', NULL, 3),
(43, 'Equipo de soldadura MIC/TIC', NULL, 3),
(44, 'Impresora de transferencia térmica', NULL, 3),
(45, 'Impresora de sobremesa para productos', NULL, 3),
(46, 'Plotter impresión UV gran formato', NULL, 3),
(47, 'Tabla digitalizadora/graficadora', NULL, 3),
(48, 'Imacs', NULL, 4),
(49, 'Workstation', NULL, 4),
(50, 'Tabletas digitalizadoras', NULL, 4),
(51, 'Pantallas interactivas', NULL, 4),
(52, 'Gafas de realidad virtual', NULL, 4),
(53, 'Dispositivos móviles', NULL, 4),
(54, 'Brazalete control de gestos', NULL, 4),
(55, 'Mocap impulse', NULL, 4),
(56, 'Beacons estimote', NULL, 4),
(57, 'Desarrollo de software', NULL, 4),
(58, 'Cámaras digitales', NULL, 4);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ats_transversal_services`
--

CREATE TABLE `ats_transversal_services` (
  `id` int(11) NOT NULL,
  `line_id` int(11) NOT NULL,
  `service_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish_ci COMMENT='Tabla intermedia para describir que uno o varios servicios pueden aplicar a varias líneas';

--
-- Volcado de datos para la tabla `ats_transversal_services`
--

INSERT INTO `ats_transversal_services` (`id`, `line_id`, `service_id`) VALUES
(1, 1, 1),
(2, 1, 2),
(3, 1, 3),
(4, 1, 4),
(5, 1, 5),
(6, 1, 6),
(7, 1, 7),
(8, 1, 8),
(9, 1, 9),
(10, 1, 10),
(11, 1, 11),
(12, 1, 12),
(13, 1, 13),
(14, 1, 14),
(15, 1, 15),
(16, 1, 16),
(17, 1, 17),
(18, 1, 18),
(19, 1, 19),
(20, 1, 20),
(21, 1, 21),
(22, 1, 22),
(23, 1, 23),
(24, 1, 24),
(25, 1, 25),
(26, 2, 26),
(27, 2, 27),
(28, 2, 28),
(29, 2, 29),
(30, 2, 30),
(31, 2, 31),
(32, 2, 32),
(33, 2, 33),
(34, 2, 34),
(35, 2, 35),
(36, 2, 36),
(37, 2, 37),
(38, 2, 38),
(39, 2, 39),
(40, 2, 40),
(41, 2, 41),
(42, 3, 42),
(43, 3, 43),
(44, 3, 44),
(45, 3, 45),
(46, 3, 46),
(47, 3, 47),
(48, 3, 48),
(49, 3, 49),
(50, 3, 50),
(51, 3, 51),
(52, 3, 52),
(54, 3, 53),
(55, 3, 54),
(56, 3, 55),
(57, 3, 56),
(58, 3, 57),
(59, 3, 58),
(60, 4, 59),
(61, 4, 60),
(62, 4, 61),
(63, 4, 62),
(64, 4, 63),
(65, 4, 64),
(66, 4, 65),
(67, 4, 66),
(68, 4, 67),
(69, 4, 68),
(70, 4, 69),
(71, 4, 70),
(72, 4, 71),
(73, 4, 72),
(74, 4, 73),
(75, 4, 74);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `userregisters`
--

CREATE TABLE `userregisters` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `city` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `departament` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `redPark` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `typemembership` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `typecompany` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sectorcompany` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `help` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `userregisters`
--

INSERT INTO `userregisters` (`id`, `name`, `email`, `city`, `departament`, `redPark`, `typemembership`, `typecompany`, `sectorcompany`, `help`, `created_at`, `updated_at`) VALUES
(1, 'alberto perez', 'alberto@gmail.es', 'medellin', 'antioquia', 'Evento Tecnoparque', 'Empresario', 'micro', 'Plásticos, Cauchos y polímeros en general', 'Cómo presentar mis ideas', NULL, NULL),
(2, 'daniel|', 'daniel@correo.es', 'bogota', 'cundinamarca', 'Internet', 'Empresario', 'big', 'Químico', 'Información sobre convocatorias', NULL, NULL),
(3, 'juan peres', 'juanp@gmail.com', 'medellin', 'antioquia', 'Evento Tecnoparque', 'Persona Natural', 'small', 'Farmacéutico', 'Cómo presentar mis ideas', NULL, NULL),
(4, 'dssdfsdsd', 'sdfsdf@dsfsf.fff', 'sdfsdfsd', 'sdfsdfdsf', 'Evento Tecnoparque', 'Empresario', 'small', 'Comercio', 'Información sobre convocatorias', NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `rol` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT 'concurrent',
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `rol`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'administrador', 'admin@correo.com', NULL, '$2y$10$LEPBCmraLycc2w41B0A6PeFkZQpelhqukeLhuqb7sYEsja37CAxeO', 'administrator', NULL, '2020-08-05 06:29:20', '2020-08-05 06:29:20'),
(2, 'user1', 'user1@hnmail.com', NULL, '$2y$10$2bvk8mZMAORW38GP8bPGwu3/wIt4U/aq55h4vKAOxx7sh5SHwUnSG', 'concurrent', NULL, '2020-08-05 06:44:05', '2020-08-05 06:44:05'),
(3, 'usuario', 'user@fmail.com', NULL, '$2y$10$pzVz1dG.wOUud5JiXtmjGex6yt5yij3yySXTmCpd3pWW1x45xb/hK', 'concurrent', 'qDWivvdYRUpDxRLg1TZCFdhxbp3MQzx1gvUWadjtJsVr1SlSyyc5fJu0HvRE', '2020-08-05 07:45:55', '2020-08-05 07:45:55');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `ats_chl_challenges`
--
ALTER TABLE `ats_chl_challenges`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_mdl_chl_challenges_users1_idx` (`idusers`);

--
-- Indices de la tabla `ats_chl_conditions`
--
ALTER TABLE `ats_chl_conditions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_mdl_chl_conditions_mdl_chl_challenge1_idx` (`id_challenge`);

--
-- Indices de la tabla `ats_chl_contacts`
--
ALTER TABLE `ats_chl_contacts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_mdl_chl_contacts_mdl_chl_challenge1_idx` (`id_challenge`);

--
-- Indices de la tabla `ats_chl_files`
--
ALTER TABLE `ats_chl_files`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_mdl_chl_files_mdl_chl_challenge1_idx` (`id_challenge`),
  ADD KEY `fk_mdl_chl_files_mdl_chl_solutions1_idx` (`id_solution`);

--
-- Indices de la tabla `ats_chl_restrictions`
--
ALTER TABLE `ats_chl_restrictions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_mdl_chl_restrictions_mdl_chl_challenge1_idx` (`id_challenge`);

--
-- Indices de la tabla `ats_chl_solutions`
--
ALTER TABLE `ats_chl_solutions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_mdl_chl_solutions_mdl_chl_challenge1_idx` (`id_challenge`),
  ADD KEY `fk_mdl_chl_solutions_users1_idx` (`idusers`);

--
-- Indices de la tabla `ats_lines`
--
ALTER TABLE `ats_lines`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `ats_lines_in_techoparks`
--
ALTER TABLE `ats_lines_in_techoparks`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_ats_lines_has_ats_technoparks_ats_technoparks1_idx` (`technopark_id`),
  ADD KEY `fk_ats_lines_has_ats_technoparks_ats_lines_idx` (`line_id`);

--
-- Indices de la tabla `ats_projects`
--
ALTER TABLE `ats_projects`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_ats_projects_ats_lines1_idx` (`line_id`);

--
-- Indices de la tabla `ats_services`
--
ALTER TABLE `ats_services`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `ats_sublines`
--
ALTER TABLE `ats_sublines`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_ats_sublines_ats_lines1_idx` (`line_id`);

--
-- Indices de la tabla `ats_technoparks`
--
ALTER TABLE `ats_technoparks`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `ats_tools`
--
ALTER TABLE `ats_tools`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_ats_tools_ats_lines1_idx` (`line_id`);

--
-- Indices de la tabla `ats_transversal_services`
--
ALTER TABLE `ats_transversal_services`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_ats_lines_has_ats_services_ats_services1_idx` (`service_id`),
  ADD KEY `fk_ats_lines_has_ats_services_ats_lines1_idx` (`line_id`);

--
-- Indices de la tabla `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `userregisters`
--
ALTER TABLE `userregisters`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `ats_chl_challenges`
--
ALTER TABLE `ats_chl_challenges`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `ats_chl_conditions`
--
ALTER TABLE `ats_chl_conditions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `ats_chl_contacts`
--
ALTER TABLE `ats_chl_contacts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `ats_chl_files`
--
ALTER TABLE `ats_chl_files`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `ats_chl_restrictions`
--
ALTER TABLE `ats_chl_restrictions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `ats_chl_solutions`
--
ALTER TABLE `ats_chl_solutions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `userregisters`
--
ALTER TABLE `userregisters`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `ats_chl_challenges`
--
ALTER TABLE `ats_chl_challenges`
  ADD CONSTRAINT `fk_mdl_chl_challenges_users1` FOREIGN KEY (`idusers`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `ats_chl_conditions`
--
ALTER TABLE `ats_chl_conditions`
  ADD CONSTRAINT `fk_mdl_chl_conditions_mdl_chl_challenge1` FOREIGN KEY (`id_challenge`) REFERENCES `ats_chl_challenges` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `ats_chl_contacts`
--
ALTER TABLE `ats_chl_contacts`
  ADD CONSTRAINT `fk_mdl_chl_contacts_mdl_chl_challenge1` FOREIGN KEY (`id_challenge`) REFERENCES `ats_chl_challenges` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `ats_chl_files`
--
ALTER TABLE `ats_chl_files`
  ADD CONSTRAINT `fk_mdl_chl_files_mdl_chl_challenge1` FOREIGN KEY (`id_challenge`) REFERENCES `ats_chl_challenges` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_mdl_chl_files_mdl_chl_solutions1` FOREIGN KEY (`id_solution`) REFERENCES `ats_chl_solutions` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `ats_chl_restrictions`
--
ALTER TABLE `ats_chl_restrictions`
  ADD CONSTRAINT `fk_mdl_chl_restrictions_mdl_chl_challenge1` FOREIGN KEY (`id_challenge`) REFERENCES `ats_chl_challenges` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `ats_chl_solutions`
--
ALTER TABLE `ats_chl_solutions`
  ADD CONSTRAINT `fk_mdl_chl_solutions_mdl_chl_challenge1` FOREIGN KEY (`id_challenge`) REFERENCES `ats_chl_challenges` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_mdl_chl_solutions_users1` FOREIGN KEY (`idusers`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `ats_lines_in_techoparks`
--
ALTER TABLE `ats_lines_in_techoparks`
  ADD CONSTRAINT `fk_ats_lines_has_ats_technoparks_ats_lines` FOREIGN KEY (`line_id`) REFERENCES `ats_lines` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_ats_lines_has_ats_technoparks_ats_technoparks1` FOREIGN KEY (`technopark_id`) REFERENCES `ats_technoparks` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `ats_projects`
--
ALTER TABLE `ats_projects`
  ADD CONSTRAINT `fk_ats_projects_ats_lines1` FOREIGN KEY (`line_id`) REFERENCES `ats_lines` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `ats_sublines`
--
ALTER TABLE `ats_sublines`
  ADD CONSTRAINT `fk_ats_sublines_ats_lines1` FOREIGN KEY (`line_id`) REFERENCES `ats_lines` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `ats_tools`
--
ALTER TABLE `ats_tools`
  ADD CONSTRAINT `fk_ats_tools_ats_lines1` FOREIGN KEY (`line_id`) REFERENCES `ats_lines` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `ats_transversal_services`
--
ALTER TABLE `ats_transversal_services`
  ADD CONSTRAINT `fk_ats_lines_has_ats_services_ats_lines1` FOREIGN KEY (`line_id`) REFERENCES `ats_lines` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_ats_lines_has_ats_services_ats_services1` FOREIGN KEY (`service_id`) REFERENCES `ats_services` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
