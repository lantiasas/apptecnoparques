<?php

function agregar_zip($dir, $zip){
  //verificamos si $dir es un directorio
  if(is_dir($dir)){
    //abrimos el directorio y lo asignamos a $da
    if ($da = opendir($dir)){
      //leemos del directorio hasta que termine
      while(($archivo = readdir($da)) !== false){
        /*Si es un directorio imprimimos la ruta
         * y llamamos recursivamente esta función
         * para que verifique dentro del nuevo directorio
         * por mas directorios o archivos
        */
        if(is_dir($dir . $archivo) && $archivo != "." && $archivo != ".."){
          echo "<strong>Creando directorio: $dir$archivo</strong><br/>";
          agregar_zip($dir . $archivo . "/", $zip);
          /*si encuentra un archivo imprimimos la ruta donde se encuentra
           * y agregamos el archivo al zip junto con su ruta 
           */
          }else
            if(is_file($dir . $archivo) && $archivo != "." && $archivo != ".."){
              //echo "Agregando archivo: $archivo <br/>";
              $zip->addFile($dir . $archivo, ''. $archivo);
            }
        }
        //cerramos el directorio abierto en el momento
        closedir($da);
    }
  }
}

class DownloadZip{

   public function Download(){
    $ruta = $this->ruta;
    $nameFile = $this->nameFile;
    $nameFolder = $this->nameFolder;
    $STRrutaFinal  = $this->STRrutaFinal;
    //fin de la función
    //creamos una instancia de ZipArchive
    $zip = new ZipArchive();
  
    /*directorio a comprimir
    * la barra inclinada al final es importante
    * la ruta debe ser relativa no absoluta
    */
    $dir = '../'.$nameFolder.''.$ruta.'/';

    //$dir = '../filesCompany/challenge'.$ruta.'/';
  
    //ruta donde guardar los archivos zip, ya debe existir
    $rutaFinal = '../'.explode('/', $nameFolder)[0].'/'.$STRrutaFinal.'';
  
    $archivoZip = $nameFile.".zip";
  
    if($zip->open($archivoZip, ZIPARCHIVE::CREATE) === true){
      agregar_zip($dir, $zip);
      $zip->close();
    
      //unlink($archivoZip);
      //Muevo el archivo a una ruta
      //donde no se mezcle los zip con los demas archivos
      rename($archivoZip, "$rutaFinal/$archivoZip");
  
      //Hasta aqui el archivo zip ya esta creado
      //Verifico si el archivo ha sido creado
      if(file_exists($rutaFinal. "/" . $archivoZip)){
        echo "fileOK";
        //echo "</br><label class='control-label'>Descargar:</label></br><a href='$rutaFinal/$archivoZip'>$nameFile</a>";
      }else{
        echo "fileError";
      }
    }
  }
}

if(isset($_POST["ruta"]) && isset($_POST["nameFile"]) && isset($_POST["nameFolder"]) && isset($_POST["STRrutaFinal"])){
    $downloadZip = new DownloadZip();
    $downloadZip -> ruta = $_POST["ruta"];
    $downloadZip -> nameFile = $_POST["nameFile"];
    $downloadZip -> nameFolder = $_POST["nameFolder"];
    $downloadZip -> STRrutaFinal = $_POST["STRrutaFinal"];
    $downloadZip -> Download();
}
?>