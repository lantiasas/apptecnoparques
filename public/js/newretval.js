function uploadArchive(){

    var sum = 0;
    var file = document.getElementById('file-input');

    //validar número limitado de archivos
    if(file.files.length > 5){
        alert('No se puede seleccionar más de 5 archivos');
        file.value = '';
    }
    for(var i=0; i<file.files.length; i++){
                
        var archive = file.files[i];
        /*if(validString(archive["name"])==false){
            file.value = '';
            alert(archive["name"] +' no tiene un nombre válido.\nDebe de tener solo letras, números y sin espacios');
            return false;
        }*/
        //archivo no tenga nombres con ma de 50 caracteres
        if(file.files[i]['name'].length > 50){
            alert(file.files[i]['name'] +' tiene un nombre demasiado largo.\n No debe de tener un nombre con más de 50 caracteres');
            file.value = '';
            return false;
        }
        //validar formato de archivos 
        if(archive["type"] !="image/jpg" &&  archive["type"] !="image/jpeg" && archive["type"] !="image/png" && archive["type"] !="application/pdf" && archive["type"] !="application/vnd.openxmlformats-officedocument.wordprocessingml.document" && 
           archive["type"] !="application/msword" && archive["type"] !="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" && archive["type"] !="application/vnd.ms-excel" && archive["type"] !="application/vnd.openxmlformats-officedocument.presentationml.presentation" &&
           archive["type"] !="application/vnd.ms-powerpoint"){
            file.value = '';
            alert(archive["name"] +' no es un archivo válido.');
        }
        sum +=archive["size"];
        //validar que el archivo no supere 2MB
        if(archive["size"] > 5242880){
            file.value = '';
            alert('El archivo '+archive["name"]+' tiene '+tamanoArchivo(archive["size"]) + '\nEl archivo no debe pesar más de 5 MB');
        }
    }
}

function newChl(e){

    e.preventDefault();
    
    if(checkContact() == false){
        if(validateFieldsCh() == true && validateRestrictions() == true && validateConditions() == true){
            document.getElementById("frmnewret").submit();
        }
    }else{
        if(validateFieldsCh() == true && validateContacts() == true && validateRestrictions() == true && validateConditions() == true){
            document.getElementById("frmnewret").submit();
        }
    }
}

//validación campos
function validateFieldsCh(){

    if(document.getElementById("descriptionret").value == ""){
        alert('Ingresar descripción');
        document.getElementById("descriptionret").focus();
        return false;
    }
    if(document.getElementById("scoperet").value == ""){
        alert('Ingresar alcance');
        document.getElementById("scoperet").focus();
        return false;
    }
    if(document.getElementById("deadlineret").value == ""){
        alert('Ingresar tiempo limite');
        document.getElementById("deadlineret").focus();
        return false;
    }else{
        return true;
    }
}

//validar campos de restricción
function validateRestrictions(){

    var itemsRestrictions = document.getElementsByName('restrictionret[]');
    for(var i = 0; i <itemsRestrictions.length; i++){
        if(itemsRestrictions[i].value == ''){
            itemsRestrictions[i].focus();
            alert('Ingresar restricción');
            return false;
        }
    }
    return true;
}

//validar campos de condición
function validateConditions(){

    var itemsConditions = document.getElementsByName('conditionsret[]');
    for(var i = 0; i <itemsConditions.length; i++){
        if(itemsConditions[i].value == ''){
            itemsConditions[i].focus();
            alert('Ingresar condición');
            return false;
        }
    }
    return true;
}

//validar campos de contacto
function validateContacts(){

    var nameContact = document.getElementsByName('nameret[]');
    var emailContact = document.getElementsByName('emailret[]');
    var telephoneContact = document.getElementsByName('phoneret[]');

    for(var i = 0; i <nameContact.length; i++){
        if(nameContact[i].value == ''){
            nameContact[i].focus();
            alert('Ingresar nombre');
            return false;
        }
    }

    for(var i = 0; i <emailContact.length; i++) {
        if(emailContact[i].value == ''){
            emailContact[i].focus();
            alert('Ingresar correo');
            return false;
        }
        if(validateEmail(emailContact[i].value) == false){
            emailContact[i].value = '';
            emailContact[i].focus();
            alert("La dirección de email es incorrecta");
            return false;
        }
    }

    for(var i = 0; i <telephoneContact.length; i++) {
        if(telephoneContact[i].value == ''){
            telephoneContact[i].focus();
            alert('Ingresar teléfono');
            return false;
        }
    }
    return true;
}

function checkContact(){

    var nameContact = document.getElementsByName('nameret[]');
    var emailContact = document.getElementsByName('emailret[]');
    var telephoneContact = document.getElementsByName('phoneret[]');
    var positionContact = document.getElementsByName('positionret[]');
    var cont = 0;
    for(var i = 0; i <nameContact.length; i++){

        if(nameContact[i].value != ''){
            cont++;
        }
        if(emailContact[i].value != ''){
            cont++;
        }
        if(telephoneContact[i].value != ''){
            cont++;
        }
        if(positionContact[i].value != ''){
            cont++;
        }
    }
    if(cont > 0){
        return true;
    }else{
        return false;
    }
}
//valida correo
function validateEmail(email) {
    if (/^[a-zA-Z0-9{.\-\_}]{1,}\@[a-zA-Z]{1,}\.[a-zA-Z]{2,3}/.test(email)){
        return true;
    }else{
        return false;
    }
}