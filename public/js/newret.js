var cont = 2;
var cont1 = 2;
var cont2 = 2;
//agregar campos condiciones
function addInputConditios(e){

    e.preventDefault();
    
    var addcn = document.getElementById("addcn");
    var div = document.createElement("div");
    var div1 = document.createElement("div");
    div.setAttribute("class", "itemtxinadd");
    div.setAttribute("id", "addinput"+cont+"");
    addcn.after(div);

    var addinput = document.getElementById("addinput"+cont+"");
    var input = document.createElement("input");
    var btn = document.createElement("button");
    input.setAttribute("type", "text");
    input.setAttribute("id", "conditionsret[]");
    input.setAttribute("name", "conditionsret[]");
    input.setAttribute("class", "inputStyle");
    input.setAttribute("autocomplete", "off");
    
    btn.setAttribute("type", "button");
    btn.setAttribute("id", "btnaddcn");
    btn.setAttribute("name", "btnaddcn");
    btn.setAttribute("class", "btn-default btnquitcond");
    btn.setAttribute("style", "outline: none;");
    btn.setAttribute("onclick", "removecn(event, " + cont + ")");
    btn.innerHTML="-";

    addinput.appendChild(input);
    addinput.appendChild(btn);
    cont++;
}

//se elimina fila seleccionada
function removecn(e, cont){

    e.preventDefault();
    var div = document.getElementById("addinput"+cont);
    div.parentNode.removeChild(div);
}

//agregar campos restricciones
function addInputRestriction(e){

    e.preventDefault();
    
    var addcn = document.getElementById("addrs");
    var div = document.createElement("div");
    div.setAttribute("class", "itemtxinadd");
    div.setAttribute("id", "addinputrs"+cont1+"");
    addcn.after(div);

    var addinputrs = document.getElementById("addinputrs"+cont1+"");
    var input = document.createElement("input");
    var btn = document.createElement("button");
    input.setAttribute("type", "text");
    input.setAttribute("id", "restrictionret[]");
    input.setAttribute("name", "restrictionret[]");
    input.setAttribute("class", "inputStyle");
    input.setAttribute("autocomplete", "off");
    
    btn.setAttribute("type", "button");
    btn.setAttribute("id", "btnaddrs");
    btn.setAttribute("name", "btnaddrs");
    btn.setAttribute("class", "btn-default btnquitres");
    btn.setAttribute("style", "outline: none;");
    btn.setAttribute("onclick", "removers(event, " + cont1 + ")");
    btn.innerHTML="-";
    addinputrs.appendChild(input);
    addinputrs.appendChild(btn);
    cont1++;
}

//se elimina fila seleccionada
function removers(e, cont){

    e.preventDefault();
    var div = document.getElementById("addinputrs"+cont);
    div.parentNode.removeChild(div);
}

//agregar campos contactos
function addInputContacs(e){
    
    e.preventDefault();
    
    var addcnt = document.getElementById("addcnt");
    var div = document.createElement("div");
    var div1 = document.createElement("div");
    div.setAttribute("class", "itemtxincntadd");
    div1.setAttribute("class", "backadd");
    div.setAttribute("id", "addinputcnt"+cont2+"");
    addcnt.after(div1);
    div1.appendChild(div);
    var addinputcnt = document.getElementById("addinputcnt"+cont2+"");

    let inputs = ["nameret[]", "emailret[]", "phoneret[]", "positionret[]"]
    let names = ["Nombre", "Correo", "Teléfono", "Posición"]
    for(let index = 0; index < 4; index++) {

        var pr = document.createElement("p");
        pr.setAttribute("class", "suntittlecnadd");
        pr.innerHTML=names[index];
        addinputcnt.appendChild(pr);
    
        var inputcnt = document.createElement("input");
        inputcnt.setAttribute("type", "text");
        inputcnt.setAttribute("id", inputs[index]);
        inputcnt.setAttribute("name", inputs[index]);
        inputcnt.setAttribute("class", "inputStylegrpadd");
        inputcnt.setAttribute("autocomplete", "off");
        addinputcnt.appendChild(inputcnt);
        
    }
    var btn = document.createElement("button");
    btn.setAttribute("type", "button");
    btn.setAttribute("id", "btnaddcnt");
    btn.setAttribute("name", "btnaddcnt");
    btn.setAttribute("class", "btn-default btnquitctn");
    btn.setAttribute("style", "outline: none; top: 65px;");
    btn.setAttribute("onclick", "removecnt(event, " + cont2 + ")");
    btn.innerHTML="-";
    addinputcnt.appendChild(btn);
    //btn.after(addinputcnt);
    //addinputcnt.after(btn);
    
    var hr = document.createElement("hr");
    hr.setAttribute("class", "brlineadd");
    addinputcnt.appendChild(hr);
    cont2++;
}

//se elimina fila seleccionada
function removecnt(e, cont){

    e.preventDefault();
    var div = document.getElementById("addinputcnt"+cont);
    div.parentNode.removeChild(div);
}

/****/

var backurl =  document.getElementById('backurl');
backurl.setAttribute("style", "text-decoration:none;");
backurl.setAttribute("href", "innv");

var infopr = document.getElementById("infopr");
infopr.innerHTML = "<span style='font-size:30px;color:#FFFFFF'>Retos";

var menuitem =  document.getElementById('menuitem');
var div = document.createElement("div");
var a = document.createElement("a");
var p = document.createElement("p");
p.setAttribute("style","font-size: 25px; color:#FFFFFF");
p.innerHTML = "Cerrar sesión";
div.setAttribute("style", "height: 40px; width: 155px; z-index: 999; margin: 0px 0px 0px -155px;");
div.setAttribute("class", "menuclose");
a.setAttribute("style", "text-decoration:none;");
a.setAttribute("href","#");
a.setAttribute("onclick","logout(event)");
menuitem.append(div);
div.appendChild(a);
a.appendChild(p);

function logout(e){
    e.preventDefault();
    document.getElementById('logout-form').submit();
}