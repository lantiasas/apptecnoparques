//permite solo números
function Numeros(e){
    tecla = (document.all) ? e.keyCode : e.which;
    //Tecla de retroceso para borrar, siempre la permite
    if(tecla==8){
      return true;
    }
    // Patron de entrada, en este caso solo acepta numeros, espacio y <<->>
    patron =/[0-9]/;
    tecla_final = String.fromCharCode(tecla);
    return patron.test(tecla_final);
}

//permite solo letras espacios
function number_letters_esp(e){
    tecla = (document.all) ? e.keyCode : e.which;
    //Tecla de retroceso para borrar, siempre la permite
    if (tecla==8){
        return true;
    }
    // Patron de entrada, en este caso solo acepta numeros, letras, espacio y . , ; :
    patron =/^[a-zñA-ZÑ\s\+\-\*\/\$\(\)\[\]\{\},.:;\áéíóúÁÉÍÓÚ0-9]{1,}?$/;
    tecla_final = String.fromCharCode(tecla);
    return patron.test(tecla_final);
}