let map = L.map('map',{
    center:['4.7889846','-73.6150235'],
    zoom:5,
    scrollWheelZoom:true,
});

L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
}).addTo(map);

let data = document.getElementById('loct').value;
data = JSON.parse(data);

for (var i=0; i<data.length; i++) {
           

    var markerLocation = new L.LatLng(data[i].latitude, data[i].longitude);
    var marker = new L.Marker(markerLocation);
    map.addLayer(marker);
    marker.bindPopup('<b>'+data[i].name+'</b>'+'<br>'+data[i].formation_center+'<br>'+data[i].address+'<br><a href="'+data[i].link+'" target="_blank">Visitar </a>');

}

function MyFunction(latitude,longitude){
    map.setView([latitude, longitude], 15);
}