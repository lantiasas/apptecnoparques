
var modal = document.getElementById("myModal");

var btn = document.getElementById("helpmodal");

var btn1 = document.getElementById("rgmodal");

var btn2 = document.getElementById("opmodal");

var btn3 = document.getElementById("innpmodal");

var span = document.getElementsByClassName("closep")[0];

var text = document.getElementById("textct");
var text1 = document.getElementById("textctsb");
var img = document.getElementById("imagetl");
var text2 = document.getElementById("textctits");

btn.onclick = function() {
  modal.style.display = "block";
  if(btn.id == 'helpmodal'){
    text1.innerHTML="Conoce LOS SERVICIOS QUE SE PRESTAN<br>A TRAVÉS DE LA RED TECNOPARQUE en<br>4 líneas tecnológicas.";
    text.innerHTML="Cómo te podemos ayudar";
    img.setAttribute('src','images/SUB-Comotepodemosayudar.png');
    text2.innerHTML="Biotecnología y nanotecnología<br>Electrónica y telecomunicaciones<br>Ingeniería y diseño<br>Tecnologías virtuales";
  }
}

btn1.onclick = function() {
  modal.style.display = "block";
  if(btn1.id == 'rgmodal'){
    text1.innerHTML="Tienes una idea para prototipar o validar?.<br>Regístrala aquí<br>";
    text.innerHTML="Registra<br>tu idea";
    img.setAttribute('src','images/SUBRegistratuidea.png');
  }
}

btn2.onclick = function() {
  modal.style.display = "block";
  if(btn2.id == 'opmodal'){
    text1.innerHTML="Conoce tendencias y tecnologías de<br>sectores de clase mundial<br>";
    text.innerHTML="Oportunidades<br>e ideas</p>";
    img.setAttribute('src','images/SUBOportunidadeseIdeas.png');
  }
}

/*btn3.onclick = function() {
  modal.style.display = "block";
  if(btn3.id == 'innpmodal'){
    text1.innerHTML="Conoce y postula retos y presenta<br>soluciones a problemáticas empresariales<br>";
    text.innerHTML="Innovación<br>abierta</p>";
    img.setAttribute('src','images/SUBInovaionAbierta.png');
  }
}*/

span.onclick = function() {
  modal.style.display = "none";
}

window.onclick = function(event) {
  if (event.target == modal) {
    modal.style.display = "none";
  }
}