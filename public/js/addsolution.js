function newsolution(e){
    
    e.preventDefault();

    if(document.getElementById("descriptionsln").value == ""){
        alert('Ingresar solución');
    }else{
        document.getElementById("frmaddsolution").submit();
    }
}

function uploadArchive(){

    var sum = 0;
    var file = document.getElementById('file-input');

    //validar número limitado de archivos
    if(file.files.length > 5){
        alert('No se puede seleccionar más de 5 archivos');
        file.value = '';
    }
    for(var i=0; i<file.files.length; i++){
                
        var archive = file.files[i];
        /*if(validString(archive["name"])==false){
            file.value = '';
            alert(archive["name"] +' no tiene un nombre válido.\nDebe de tener solo letras, números y sin espacios');
            return false;
        }*/
        //archivo no tenga nombres con ma de 50 caracteres
        if(file.files[i]['name'].length > 50){
            alert(file.files[i]['name'] +' tiene un nombre demasiado largo.\n No debe de tener un nombre con más de 50 caracteres');
            file.value = '';
            return false;
        }
        //validar formato de archivos 
        if(archive["type"] !="image/jpg" &&  archive["type"] !="image/jpeg" && archive["type"] !="image/png" && archive["type"] !="application/pdf" && archive["type"] !="application/vnd.openxmlformats-officedocument.wordprocessingml.document" && 
           archive["type"] !="application/msword" && archive["type"] !="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" && archive["type"] !="application/vnd.ms-excel" && archive["type"] !="application/vnd.openxmlformats-officedocument.presentationml.presentation" &&
           archive["type"] !="application/vnd.ms-powerpoint"){
            file.value = '';
            alert(archive["name"] +' no es un archivo válido.');
        }
        sum +=archive["size"];
        //validar que el archivo no supere 2MB
        if(archive["size"] > 5242880){
            file.value = '';
            alert('El archivo '+archive["name"]+' tiene '+tamanoArchivo(archive["size"]) + '\nEl archivo no debe pesar más de 5 MB');
        }
    }
}


/********* */
var url =  document.getElementById('url').value;
var backurl =  document.getElementById('backurl');
backurl.setAttribute("style", "text-decoration:none;");
backurl.setAttribute("href", url);

var infopr = document.getElementById("infopr");
infopr.innerHTML = "<span style='font-size:30px;color:#FFFFFF'>Retos";

var menuitem =  document.getElementById('menuitem');
var div = document.createElement("div");
var a = document.createElement("a");
var p = document.createElement("p");
p.setAttribute("style","font-size: 25px; color:#FFFFFF");
p.innerHTML = "Cerrar sesión";
div.setAttribute("style", "height: 40px; width: 155px; z-index: 999; margin: 0px 0px 0px -155px;");
div.setAttribute("class", "menuclose");
a.setAttribute("style", "text-decoration:none;");
a.setAttribute("href","#");
a.setAttribute("onclick","logout(event)");
menuitem.append(div);
div.appendChild(a);
a.appendChild(p);

function logout(e){
    e.preventDefault();
    document.getElementById('logout-form').submit();
}