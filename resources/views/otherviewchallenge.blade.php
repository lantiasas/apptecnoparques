@extends('templatetec')
@extends('headermenu')
<link rel="stylesheet" href="{{asset('plugins/viewchallenge.css')}}">
@section('headerTittle')
<a href="innv" style="color: white;">
    Retos
</a>
@endsection

@section('content')
<div class="ContentItemsin">
    <div class="PrTittle"></div>
    <div class="triangle"></div>
    <div class="itemin">
        <img src="images/verretos.png" class="imageretinleft">
        <p class="font-weight-bold text-center prretleft" >
            Otros retos
        </p>
    </div>

    <div class="iteminview">
        <div class="tblsize">
            <table class="table">
                @foreach($otherviewchl as $value)
                <tr>
                    @php
                        $nm = '';
                        if($value->nr == ''){
                            $nm = 'Reto #'.$value->id;
                        }else {
                            $nm = $value->nr;
                        }
                    @endphp
                    <td class="cnt">{{$nm}}</td>
                    <!--<td>{{$value->description}}</td>-->
                    <td>
                        <form action="viewselectchl" method="post" enctype="multipart/form-data" id="frmviewchl">
                            {{@csrf_field()}}
                            <input type="hidden" name="idchl" value="{{$value->id}}">
                            <button type="submit" class="btnchl">
                                <img src="images/view.png" class="imglinkview">
                            </button>
                        </form>
                    </td>
                </tr>
                @endforeach
            </table>
        </div>
    </div>
    <div class="iteminview">
        <div class="pgcenter">
            {{ $otherviewchl->links() }}
        </div>
    </div>
</div>
<script src="{{asset('js/otherviewchallenge.js')}}"></script>
@endsection