@extends('templatetec')
@extends('headermenu')
<link rel="stylesheet" href="{{asset('plugins/viewsolution.css')}}">
@section('headerTittle')
<a href="innv" style="color: white;">
    Retos
</a>
@endsection

@section('content')
<div class="ContentItemsin">
    <div class="PrTittle"></div>
    <div class="triangle"></div>
    <div class="itemin">
        <img src="images/verretos.png" class="imageretinleft">
        <p class="font-weight-bold text-center prretleft" >
            Ver soluciones
        </p>
    </div>

    <div class="iteminview">
        <div class="tblsz">
            <table class="table">
                @foreach($solutionview as $value)
                <tr>
                    <td class="cnt_">{{'Solución #'.$value->id}}</td>
                    <td class="cnt">{{$value->username}}</td>
                    <td class="cntview">
                        @php
                            if($rol == 'administrator'){
                                echo '<form action="viewslnslt" method="post" enctype="multipart/form-data" id="frmviewchl">';
                                }
                            else {
                                echo '<form action="viewslnsltusr" method="post" enctype="multipart/form-data" id="frmviewchl">';
                                }
                            
                        @endphp
                        
                            {{@csrf_field()}}
                            <input type="hidden" name="idsln" value="{{$value->id}}">
                            <button type="submit" style="border: none;">
                                <img src="images/view.png" class="imglinkviewsln">
                            </button>
                        </form>
                    </td>
                </tr>
                @endforeach
            </table>
        </div>
    </div>
    <div class="iteminview">
        <div class="pgcenter">
            {!! $solutionview->appends(["idviewchl" => $idviewchl]) !!}
        </div>
    </div>
</div>
<script src="{{asset('js/viewsln.js')}}"></script>
@endsection