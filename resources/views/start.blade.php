@extends('template')
@extends('headermenu')
<link rel="stylesheet" href="{{asset('plugins/index.css')}}">
@section('tittle')
    Tecnoparques sena
@endsection

@section('header')
<div class="back">
    <img src="images/Tecnoparque_Mesa de trabajo1.png" class="imageStyle">
</div>
@endsection

@section('content')

<div class="centerContent">
    <p class="text-center centerTittle">
        Bienvenidos a la App de servicios del Tecnoparque Sena
    </p>
</div>

<div class="centerContent">
    <div class="centerContent">
        <p class="text-justify contenpr">
            El objetivo de este aplicativo web, es presentar las
            capacidades tecnológicas y servicios de la red
            Tecnoparques Sena. La red busca aportar al
            ecosistema de innovación en la mejora de su
            competitividad y productividad a través del
            acompañamiento a emprendedores y empresas que
            requieren prototipar y validar proyectos de ciencia
            tecnología e innovación.
        </P>
    </div>
</div>

<div class="centerContent">
    <div class="link">
        <a href="menu" style="text-decoration: none;">
            <p class="styleLink">
                Comencemos
            </p>
        </a>
    </div>
</div>
@endsection