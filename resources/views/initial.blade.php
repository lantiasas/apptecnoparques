@extends('template')
@section('tittle')
    Tecnoparques sena
@endsection

@section('header')
    <img src="images/Tecnoparque_Mesa de trabajo1.png" class="img-responsive" style="background-color: #098A82; display: block; margin-left: auto; margin-right: auto;">
@endsection

@section('content')
<div class="row">
    <div class="col-sm-12 d-flex justify-content-center" style="margin-top: 150px;">
        <p class="" style="font-weight: bold; font-size:54px;color: #5d5d5d;">
            Bienvenidos a la app de servicios<br>
        </p>
    </div>
    <div class="col-sm-12 d-flex justify-content-center" style="">
        <p class="" style="font-weight: bold; font-size:54px;color: #5d5d5d;">
            del Tecnoparque Sena
        </p>
    </div>
</div>
<div class="container">
    <div style="width: 987px; height: 100px; margin: 40px auto;text-align: center;">
        <p class="text-justify" style="font-size:44px; color: #5d5d5d;">
            La red Tecnoparque del Sena es un programa del
            sistema de investigación desarrollo Tecnológico e
            innovación (SENNOVA) que busca aportar al
            ecosistema de innovación en la mejora de su
            competitividad y productividad a través del
            acompañamiento a emprendedores y empresas que
            requieren prototipar y validar proyectos de ciencia
            tecnología e innovación. La red Tecnoparque tiene 16
            sedes a nivel nacional, las cuales cuentan con
            tecnología especializada y un equipo humano
            altamente capacitado en 4 áreas: biotecnología y
            nanotecnología, electrónica y telecomunicaciones,
            ingeniería y diseño y tecnologías virtuales.
        </p>
    </div>
</div>
@endsection