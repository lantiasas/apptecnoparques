@extends('template')
@extends('headermenu')
<link rel="stylesheet" href="{{asset('plugins/help.css')}}">
@section('tittle')
    Tecnoparques sena
@endsection

@section('header')
<div class="header">
  <a href="menu">
    <p class="tittleHeder">
      &#8592; Cómo te podemos ayudar
    </p>
  </a>
</div>
@endsection

@section('content')
<div class="ContentItems">
  <div class="itemTitle">
    <p class="text-justify font-weight-bold tittle">
      Conoce los servicios que se prestan a
      través de la Red Tecnoparque en 4
      líneas tecnológicas.
    </p>
  </div>
  <div class="ContentItems d-flex flex-row justify-content-around">
    <div class="item">
      <a href="biotec">
        <img src="images/P2-08.png" class="image">
          <p class="font-weight-bold text-center imageTittle" >
            Biotecnología<br>y nanotecnología
          </p>
      </a>
    </div>

    <div class="item">
      <a href="elect">
        <img src="images/P2-09.png" class="image">
        <p class="font-weight-bold text-center imageTittle" >
          Electrónica<br>y telecomunicaciones
        </p>
      </a>
    </div>

    <div class="item">
      <a href="ingd">
        <img src="images/P2-10.png" class="image">
          <p class="font-weight-bold text-center imageTittle">
            Ingeniería y diseño
          </p>
      </a>
    </div>

    <div class="item">
      <a href="tecv">
        <img src="images/P2-11.png" class="image ml-4">
          <p class="font-weight-bold text-left imageTittle mr-1">Tecnologías virtuales</p>
      </a>
    </div>
  </div>
</div>
<script src="{{asset('js/help.js')}}"></script>
@endsection