<link rel="stylesheet" href="{{asset('plugins/headermenu.css')}}">
<div class="headermenu">
    <div class="headtittle">
        <div class="vl"></div>
        <div class="vl1"></div>
        <div class="arrow">
            <a id="backurl">
                <p class="backarrow">
                    &#8592;
                </p>
            </a>
        </div>
        <div class="infotl">
            <p class="infopr" id="infopr"></p>
        </div>
    </div>
    <div class="helpnextTec">
      <p class="tittleMenu">
      </div>
    </div>
    <input type="checkbox" class="openSidebarMenu" id="openSidebarMenu">
    <label for="openSidebarMenu" class="sidebarIconToggle">
        <div class="spinner diagonal part-1"></div>
        <div class="spinner horizontal"></div>
        <div class="spinner diagonal part-2"></div>
    </label>
    <div id="sidebarMenu">
        <ul class="sidebarMenuInner">
            <div class="logo-image">
                <img src="images/Tecnoparque_Mesa de trabajo1.png" class="img-fluid imageMenu">
            </div>
        </ul>
        <div class="file" id="menuitem">
            <div class="d-flex">
                <div class="itemsMenuB">
                    <a href="start" style="text-decoration: none;">
                        <p class="itemsB">Bienvenidos ></p>
                        <hr class="borderLine" style="margin:-26px 0px 0px 47px;">
                    </a>
                </div>
                <div class="itemsMenuM">
                    <a href="menu" style="text-decoration: none;">
                        <p class="itemsM">Módulos ></p>
                        <hr class="borderLine" style="margin:-26px 0px 0px 47px;">
                    </a>
                </div>
                <div class="itemsMenuR">
                    <a href="Userregister" class="showtxt" style="text-decoration: none;">
                        <p class="itemsR">Registro ></p>
                        <p class="text-justify paragraph">
                            Regístrate para recibir información
                            de convocatorias y oportunidades de
                            la Red Tecnoparques Sena.
                        </p>
                    </a>
                    <div class="parraf">
                        <p class="text-justify">
                            Regístrate para recibir información de convocatorias y<br>
                            oportunidades de la Red Tecnoparques Sena.
                        </p>
                    </div>
                </div>
                <div class="itemsMenuC">
                    <a href="credits" style="text-decoration: none;">
                        <p class="itemsC">Créditos ></p>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>