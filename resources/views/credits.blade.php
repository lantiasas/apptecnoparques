@extends('template')
@extends('headermenu')
<link rel="stylesheet" href="{{asset('plugins/credits.css')}}">
@section('tittle')
    Tecnoparques sena
@endsection

@section('header')
<div class="header">
    <a href="menu">
        <p class="tittleHeder">
            &#8592; Créditos
        </p>
    </a>
</div>
@endsection

@section('content')
<div class="ContentItems">
    <div class="itemtl">
        <p class="tittlePr">
            <br>
            Conoce las sublíneas de trabajo
            <br>
        </p>
    </div>
    <div class="triangle_"></div>
    <div class="itemtittle">
        <p class="text-justify crParagraph">
            Esta aplicación fue desarrollada en
            el marco del proyecto Diagnóstico y
            Fortalecimiento de la Red
            Tecnoparque, auspiciado por el
            Sena y Minciencias y operado por
            VT S.A.S bajo el rol de consultor y
            apoyo metodológico.
        </p>
    </div>
    <div class="item">
        <img src="images/Logos Vt.png" class="imageCrv">
    </div>
    <div class="item">
        <img src="images/Logos Lantia.png" class="imageCrl">
    </div>

    <div class="item">
        <img src="images/mineducacion.png" class="imageCrm">
    </div>
</div>
<script src="{{asset('js/credits.js')}}"></script>
@endsection