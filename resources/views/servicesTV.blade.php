@extends('template')
@extends('headermenu')

<link rel="stylesheet" href="{{asset('collapse/accordion.css')}}">
<link rel="stylesheet" href="{{asset('plugins/servicesTV.css')}}">
<style type="text/css">
    @media screen and (max-width: 992px) {
      .accordioncenter::after{
        margin: -28px 25px 0px 0px;
      }
    }
    @media (min-width: 1024px) and (max-width: 1680px) {
      .accordioncenter::after{
        margin: -46px 380px 19px 34px;
      }
    }
</style>
@section('tittle')
    Tecnoparques sena
@endsection

@section('header')
<div class="header">
  <a href="tecv">
  <p class="tittleHeder">
    &#8592; Servicios
  </p>
</a>
</div>
@endsection

@section('content')

<div class="ContentItems">

    <div class="itemtl">
        <p class="tittlePr">
            <br>
            El tecnoparque brinda de forma gratuita asesoría técnica personalizada para:
            <br>
        </p>
        <img src="images/masicons-01.png" class="imgtl">
        <p class="text-justify tittleimg">
            Desarrollo de<br>proyectos en<br>I+D+I
        </p>
        <img src="images/masicons-02.png" class="imgtl1">
        <p class="text-justify tittleimg1">
            Adaptación y<br>transferencia<br>de tecnología
        </p>
        <img src="images/masicons-03.png" class="imgtl2">
        <p class="text-justify tittleimg2">
            Generación de<br>apropiación del<br>conocmiento en<br>una o más de las<br>capacidades
        </p>
        <img src="images/masicons-04.png" class="imgtl3">
        <p class="text-justify tittleimg3">
            Sevicios de las<br>diferentes sub<br>líneas
        </p>
    </div>
    <div class="triangle"></div>
</div>

<div class="ContentItems_">
    @php($count=0)
    @php($count1=0)
    @php($count2=0)
    @foreach ($servicesTV as $item)

        @if($item->id == '59' || $item->id == '66' || $item->id == '70')
        <div class="item">
            <button class="accordioncenter">
                <div class="centertetxt" style="font-size: 17px;">
                    {{$item->name}}
                </div>
                <hr class="lineP1">
            </button>
            <div class="panelsv">
                <br>
                <div class="textsrv">
        @endif
        
        @if($item->id >=60 && $item->id<=65)
            {{ $item->name }}<br>
            @php($count++)
        @endif

        @if($item->id >=67 && $item->id<=69)
            {{ $item->name }}<br>
            @php($count1++)
        @endif

        @if($item->id >=71 && $item->id<=74)
            {{ $item->name }}<br>
            @php($count2++)
        @endif

        @if($count==6)
        <br>
            </div>
            </div>
            </div>
            @php($count=0)
        @endif

        @if($count1==3)
        <br>
            </div>
            </div>
            </div>
            @php($count1=0)
        @endif

        @if($count2==6)
        <br>
            </div>
            </div>
            </div>
            @php($count2=4)
        @endif
    @endforeach
</div>
<script src="{{asset('collapse/accordioncenter.js')}}"></script>
<script src="{{asset('js/servicestv.js')}}"></script>
@endsection