@extends('templatetec')

@extends('headermenu')
<link rel="stylesheet" href="{{asset('plugins/innv.css')}}">
@section('headerTittle')
<a href="home" style="color: white;">
    Inovación abierta
</a>
@endsection

@section('content')
<div class="ContentItemsin">

  <div class="itemin ">
      <img src="images/retos.png" class="imagein">
      <p class="font-weight-bold text-center imageTittlein" >
        Retos
      </p>
    <div class="vertical"></div>
  </div>

  <div class="itemin">
    <div class="container">
      <div class="row">
          <div class="col-sm-6">
            <a href="newret">
              <img src="images/nuevosretos.png" class="imageretinleft">
              <p class="font-weight-bold text-center prretleft" >
                Nuevos retos
              </p>
            </a>
          </div>
          <div class="col-sm-6">
            <a href="viewchallenge">
              <img src="images/verretos.png" class="imageretinright">
              <p class="font-weight-bold text-center prretright" >
                  Ver retos
              </p>
            </a>
          </div>
      </div>
    </div>
  </div>
@php
    if($data != 'administrator'){
      echo'  <div class="iteml">
    <div class="container">
      <div class="row">
          <div class="col-sm">
            <a href="otherviewchallenge">
              <img src="images/verretos.png" class="imageretinright_">
              <p class="font-weight-bold text-center prretright1" style="left: 110px;">
                  Ver otros retos
              </p>
            </a>
          </div>
      </div>
    </div>
  </div>';
    }
@endphp
</div>
<script src="{{ asset('js/innv.js') }}"></script>
@endsection