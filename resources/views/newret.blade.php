@extends('templatetec')
@extends('headermenu')
<link rel="stylesheet" href="{{asset('plugins/newret.css')}}">
@section('headerTittle')
<a href="innv" style="color: white;">
    Retos
</a>
@endsection

@section('content')
<div class="ContentItemsin">
    <div class="PrTittle"></div>
    <div class="triangle"></div>
    <div class="itemin d-flex flex-column">
        <div class="d-flex flex-row justify-content-around align-items-center" style="position: relative; top: -80px; margin: 0px 750px">  
            <img src="images/nuevosretos.png" class="imageretinlefts" style="width: 50px; ">
            <p class="font-weight-bold text-white m-0" >
                Nuevos retos
            </p>
        </div>
    </div>
<form action="newret" method="post" enctype="multipart/form-data" id="frmnewret">
        {{@csrf_field()}}
        <div class="itemtxin">
            <p class="tittleItem">Nombre</p>
            <input type="text" id="namert" name="namert" onKeyPress="return number_letters_esp(event)" class="inputStyle" autocomplete="off">
        </div>

        <div class="itemtxin">
            <p class="tittleItem">Descripción</p>
            <input type="text" id="descriptionret" name="descriptionret" onKeyPress="return number_letters_esp(event)" class="inputStyle" autocomplete="off">
        </div>

        <div class="itemtxin">
            <p class="tittleItem">Alcance</p>
            <input type="text" id="scoperet" name="scoperet" onKeyPress="return number_letters_esp(event)" class="inputStyle" autocomplete="off">
        </div>

        <div class="itemtxin">
            <p class="tittleItem">Fecha límite</p>
            <input type="text" id="deadlineret" name="deadlineret" onKeyPress="return number_letters_esp(event)" class="inputStyle" autocomplete="off">
        </div>

        <div class="itemtxin" id="addcn">
            <p class="tittleItem">Condiciones</p>
            <input type="text" id="conditionsret[]" name="conditionsret[]" onKeyPress="return number_letters_esp(event)" class="inputStyle" autocomplete="off">
            <button class="btn-default btnadd" type="button" onclick="addInputConditios(event);" id="btnaddcn" name="btnaddcn" style="outline: none;">+</button>
        </div>

        <div class="itemtxin" id="addrs">
            <p class="tittleItem">Restricciones</p>
            <input type="text" id="restrictionret[]" name="restrictionret[]" onKeyPress="return number_letters_esp(event)" class="inputStyle" autocomplete="off">
            <button class="btn-default btnadd" type="button" onclick="addInputRestriction(event);" id="btnaddrs" name="btnaddrs" style="outline: none;">+</button>
        </div>

        <div class="itemtxincnt" id="addcnt">
            <p class="tittleItem">Contacto</p>
            <div class="back">
                <div class="centercnt">
                    <p class="suntittlecn">Nombre</p>
                    <input type="text" id="nameret[]" name="nameret[]" onKeyPress="return number_letters_esp(event)" class="inputStylegrp" autocomplete="off">
                    <p class="suntittlecn">Correo</p>
                    <input type="text" id="emailret[]" name="emailret[]" class="inputStylegrp" autocomplete="off">
                    <p class="suntittlecn">Teléfono</p>
                    <input type="text" id="phoneret[]" name="phoneret[]" onKeyPress="return Numeros(event)" class="inputStylegrp" autocomplete="off">
                    <p class="suntittlecn">Posición</p>
                    <input type="text" id="positionret[]" name="positionret[]" onKeyPress="return number_letters_esp(event)" class="inputStylegrp" autocomplete="off">
                </div>
            </div>
            <button class="btn-default btnaddct" type="button" onclick="addInputContacs(event);" id="btnaddcnt" name="btnaddcnt" style="outline: none;">+</button>
            <hr class="brline">
        </div>

        <div class="itemtxin" id="addrs">
            <p class="tittleItem">Adjuntar archivos</p>

            <div class="image-upload">
                <label for="file-input">
                    <img src="images/add.png"/>
                </label>
                <input id="file-input" type="file" name="archives[]" onchange="uploadArchive(event);" multiple=""/>
            </div>
        </div>

        <div class="itemtxin ml-5">
            <button class="btnrets" id="btnsaveret" name="btnsaveret" onclick="newChl(event)" style="outline: none;">Ingresar</button>
            <button type="button" class="btnviews" style="outline: none;" onclick="window.location='{{ route('viewchallenge') }}'">Ver retos</button>
        </div>
    </form>
</div>
<script src="{{asset('js/validations.js')}}"></script>
<script src="{{asset('js/newretval.js')}}"></script>
<script src="{{asset('js/newret.js')}}"></script>
@endsection