@extends('template')
@extends('headermenu')
<link rel="stylesheet" href="{{asset('plugins/biotec.css')}}">
@section('tittle')
    Tecnoparques sena
@endsection

@section('header')

<div class="header">
  <a href="helpnext">
    <p class="tittleHeder">
      &#8592; Biotecnología y nanotecnología
    </p>
</a>
</div>
@endsection

@section('content')
<div class="ContentItems">
  <div class="item">
    <img src="images/masicons-16-02.png" class="image">
  </div>

  <a href="location">
    <div class="linkTc">
    </div>
  </a>

  <a href="tecpark?id=1">
    <div class="linkSB">
    </div>
  </a>

  <a href="sublines">
    <div class="linklc">
    </div>
  </a>

  <a href="Infrastructure">
    <div class="linkin">
    </div>
  </a>

  <a href="services">
    <div class="linkpr">
    </div>
  </a>
    
  <a href="project?id=1">
    <div class="linksv">
    </div>
  </a>

  <a href="helpnext">
    <div class="linkbi">
    </div>
  </a>
  
</div>
<script src="{{asset('js/biotec.js')}}"></script>
@endsection