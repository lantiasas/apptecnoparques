@extends('template')
@extends('headermenu')
<link rel="stylesheet" href="{{asset('plugins/location.css')}}">
@section('tittle')
    Tecnoparques sena
@endsection

@section('header')
<div class="header">
  <a href="{{ url()->previous() }}">
    <p class="tittleHeder">
      &#8592; Dónde estamos
    </p>
  </a>
</div>
@endsection

@section('content')
<div class="ContentItems">
  <div class="item">
    <div class="itemImage" id="map">
    </div>
  </div>

  <div class="itemtl">
    <div class="itemTittle">
      <p class="tittleP">
        Ubica uno de nuestros Tecnoparques ⯆
      </p>
    </div>
  </div>

  <input type="hidden" id="loct" value="{{$loct}}">
  
  <div class="itemlg">
    <div class="container-fluid">
        @for ($i = 0; $i < count($loct); $i++)
        
          @if($i%3==0)
            <div class="row">
          @endif

          @if($i%3<3)
            <div class="col-4">
              <a class="fontstyle" href="#"  style="text-decoration: none;" onclick="MyFunction('{{$loct[$i]->latitude}}', '{{$loct[$i]->longitude}}')";return false;>{{$loct[$i]->name}}</a>
            </div>
          @endif
          
          @if($i%3==2)
            </div>
          @endif

        @endfor
    </div>
  </div>
</div>
</div>

<link rel="stylesheet" href="{{asset('leaflet/leaflet.css')}}">
<script src="{{asset('leaflet/leaflet.js')}}"></script>
<script src="{{asset('js/map.js')}}"></script>
@endsection