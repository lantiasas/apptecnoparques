@extends('template')
<link rel="stylesheet" href="{{asset('plugins/Userregisterend.css')}}">
@section('tittle')
    Tecnoparques sena
@endsection

@section('header')
<div class="header">
    <div class="imgclose">
        <a href="start" style="text-decoration: none;">
          <div class="initcls">
          </div>
        </a>
      </div>
    <a href="Userregisternext">
        <p class="tittleHeder">
        &#8592; Regístrarse
        </p>
    </a>
</div>
@endsection

@section('content')
<a href="menu">
    <div class="cross">
        &#10005;
    </div>
</a>
<p class="Tittle">
    Llena el formulario
</p>
<div class="triangle_"></div>
<form action="Userregisterend" method="POST">
    @csrf
<div class="ContentItems">
    <div class="item">
        <p class="tittleItem">Sector de la empresa</p>
        <select id="sectorcompany" name="sectorcompany" class="selectOption">
            <option value="selected">Seleccione una opción</option>
            <option value="Alimentos y Bebidas">Alimentos y Bebidas</option>
            <option value="Agroindustria">Agroindustria</option>
            <option value="Ganadería y Pecuaria">Ganadería y Pecuaria</option>
            <option value="Plásticos, Cauchos y polímeros en general">Plásticos, Cauchos y polímeros en general</option>
            <option value="Químico">Químico</option>
            <option value="Farmacéutico">Farmacéutico</option>
            <option value="Cosmético">Cosmético</option>
            <option value="Financiero">Financiero</option>
            <option value="Servicios Profesionales">Servicios Profesionales</option>
            <option value="Tecnologías de Información y Comunicación">Tecnologías de Información y Comunicación</option>
            <option value="Comercio">Comercio</option>
            <option value="Gastronomía">Gastronomía</option>
            <option value="Audiovisual, entretenimiento">Audiovisual, entretenimiento</option>
            <option value="Publicaciones">Publicaciones</option>
            <option value="Servicios públicos">Servicios públicos</option>
            <option value="Automotríz y autopartes">Automotríz y autopartes</option>
            <option value="Metalmecánica">Metalmecánica</option>
            <option value="Electrodomésticos">Electrodomésticos</option>
            <option value="Pulpa y papel">Pulpa y papel</option>
        </select>
    </div>
    <div class="item">
        <p class="tittleItem">¿Cómo te podemos ayudar?</p>
        <select multiple id="help" name="help" class="selectOption" style="height:100px;">
            <option value="Seleccione una opción">Seleccione una opción</option>
            <option value="selected"></option>
            <option value="Información sobre convocatorias">Información sobre convocatorias</option>
            <option value="Cómo presentar mis ideas">Cómo presentar mis ideas</option>
            <option value=""></option>
        </select>
    </div>
    <div class="item">
        <div class="link">
            
                <p class="itemLink">
                    <button type="submit" class="btnEnd">Registro Completo</button>
                </p>
            
        </div>
    </div>
    <div class="item">
        <div class="slideshow-container">
            <div class="mySlides">
                <a href="Userregisternext">
                    <div class="link1">
                        <p class="itemLink1"> &#x3C; Atrás</p>
                    </div>
                </a>
            </div>
        </div>

        <div style="text-align:center; margin-top:50px;">
            <span class="dot off" onclick="currentSlide(1)"></span>
            <a href="Userregisternext">
                <span class="dot off" onclick="currentSlide(2)"></span>
            </a>
            <span class="dot on" onclick="currentSlide(3)"></span> 
          </div>
    </div>
</div>
</form>
<link rel="stylesheet" href="{{asset('sliderI/sld.css')}}">
<script src="{{asset('sliderI/sld.js')}}"></script>
@endsection