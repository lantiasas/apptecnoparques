@extends('template')
@extends('headermenu')
<link rel="stylesheet" href="{{asset('plugins/helpnext.css')}}">
@section('tittle')
    Tecnoparques sena
@endsection

@section('header')
<div class="header">
  <a href="help">
    <p class="tittleHeder">
      &#8592; Cómo te podemos ayudar
    </p>
</a>
</div>
@endsection

@section('content')
<div class="ContentItems">

  @foreach ($bio as $item)
    @if($item->id== '1')
    <div class="item">
      <a href="biotec" style="text-decoration: none;">
        <img src="images/P2-08.png" class="image">
        <p class="font-weight-bold text-center imageTittle" >
            {{$item->name}}
        </p>
      </a>
    </div>
  
    <div class="item">
      <p class="text-justify P">
        {{$item->description}}
      </p>
    </div>

    @endif
  @endforeach
</div>
<script src="{{asset('js/helpnext.js')}}"></script>
@endsection