@extends('template')
@extends('headermenu')
<link rel="stylesheet" href="{{asset('plugins/biotec.css')}}">
@section('tittle')
    Tecnoparques sena
@endsection

@section('header')

<div class="header">
  <a href="helpnextEl">
    <p class="tittleHeder" style="padding: 18px;">
      &#8592; Electrónica y Telecomunicaciones
    </p>
</a>
</div>
@endsection

@section('content')
<div class="ContentItems">
  <div class="item">
    <img src="images/masicons-16-03.png" class="image">
  </div>

  <a href="location">
    <div class="linkTc">
    </div>
  </a>

  <a href="tecpark?id=2">
    <div class="linkSB">
    </div>
  </a>

  <a href="sublinesET">
    <div class="linklc">
    </div>
  </a>

  <a href="InfrastructureET?id=1">
    <div class="linkin">
    </div>
  </a>

  <a href="servicesET">
    <div class="linkpr">
    </div>
  </a>
    
  <a href="project?id=2">
    <div class="linksv">
    </div>
  </a>

  <a href="helpnextEl">
    <div class="linkbi">
    </div>
  </a>
</div>
<script src="{{asset('js/elect.js')}}"></script>
@endsection