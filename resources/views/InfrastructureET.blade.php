@extends('template')
@extends('headermenu')
<link rel="stylesheet" href="{{asset('plugins/Infrastructure.css')}}">
<link rel="stylesheet" href="{{asset('collapse/accordion.css')}}">
@section('tittle')
    Tecnoparques sena
@endsection

@section('header')
<div class="header">
  <a href="{{ url()->previous() }}">
    <p class="tittleHeder">
        &#8592; Infraestructura
    </p>
  </a> 
</div>
@endsection

@section('content')
<div class="ContentItems">
  <div class="itemtl">
    <p class="tittlePr">
        <br>
        Estos son los equipos disponibles en la<br>
        Red Tecnoparque para prototipado y<br>
        validación de productos
        <br>
    </p>
  </div>
  <div class="triangle"></div>
</div>
<div class="ContentItems">
    @foreach ($InfrastructureET as $item)
    <div class="item">
      <button class="accordion tittle_" style="font-size: 19px;">{{$item->name}}
          <hr class="line">
      </button>
      <div class="panelsv">
      </div>
  </div>
    @endforeach
</div>
<script src="{{asset('collapse/accordion.js')}}"></script>
<script src="{{asset('js/infrastructureet.js')}}"></script>
@endsection