@extends('template')
@extends('headermenu')
<link rel="stylesheet" href="{{asset('plugins/project.css')}}">
@section('tittle')
Casos de éxito
@endsection

@section('header')
<div class="header">
<a href="{{ url()->previous() }}">
  <p class="tittleHeder">
    &#8592; Casos de éxito
  </p>
 </a>
</div>
@endsection

@section('content')
<div class="PrTittle">
    <p class="text-justify">
        Conozca los Tecnoparques donde se
        cuenta con la línea biotecnología y
        nanotecnología
    </p>
</div>
<div class="triangle"></div>
<div class="ContentItems">
    <div class="itemtl">
        <p class="text-justify prParagraph">
            Conozca los Tecnoparques donde se
            cuenta con la línea biotecnología y
            nanotecnología
        </p>
    </div>
    <div class="triangle"></div>
    <div class="item">
        <p class="text-justify prItemtittle">
            Parche transdérmico
        </p>
        <img src="images/P2-13.png" class="prImage">
        <p class="text-justify prSbParagraph">
            Parche de liberación sostenida con propiedades
            analgésicas y anti inflamatorias elaborado con extractos
            de plantas medicinales del Amazonas Colombiano
        </p>
        <hr class="prLine">
    </div>

    <div class="item">
        <p class="text-justify prItemtittle">
            Cebolla orgánica
        </p>
        <img src="images/P2-14.png" class="prImage">
        <p class="text-justify prSbParagraph">
            Desarrollo de cebolla orgánica deshidratada y
            pulverizada de valor agregado en la producción.
            Tecnoparque apoyó el desarrollo del producto, las tablas
            nutricionales y el diseño de la presentación
        </p>
    </div>
</div>
<script src="{{asset('js/project.js')}}"></script>
@endsection