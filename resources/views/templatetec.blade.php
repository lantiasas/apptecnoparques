<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <!--<meta name="viewport" content="width=1275, initial-scale=1.0">-->
    <meta name="viewport" content="width=device-width, user-scalable=no">

    <link rel="stylesheet" href="{{asset('bootstrap/css/bootstrap.css')}}">
    <link rel="stylesheet" href="{{asset('plugins/menu.css')}}">
    <link rel="stylesheet" href="{{asset('plugins/size.css')}}">
    <script src="{{asset('plugins/menu.js')}}"></script>
    <link rel="stylesheet" href="{{asset('plugins/fonts.css')}}">
    <script src="{{ asset('js/app.js') }}" defer></script>
    <link rel="stylesheet" href="{{asset('plugins/innheader.css')}}">
    <title>@yield('tittle', 'default')</title>
</head>
<div class="header">
    <p class="tittleHeder">
      &#8592; @yield('headerTittle')
    </p>
</div>
<?php
if(Auth::user() == null){
    header("Location: login");
    exit;
}
?>
<div class="menuL">
  <nav class="navbar navbar-expand-md navbar-light shadow-sm" id="barNav">
    <div class="container">
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <!-- Left Side Of Navbar -->
            <ul class="navbar-nav mr-auto">

            </ul>

            <!-- Right Side Of Navbar -->
            <ul class="navbar-nav ml-auto">
                <!-- Authentication Links -->
                @guest
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                    </li>
                    @if (Route::has('register'))
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                        </li>
                    @endif
                @else
                    <li class="nav-item dropdown" style="right: 95px; top:20px;">
                        <a class="dropdown-item" href="{{ route('logout') }}"
                        onclick="event.preventDefault(); 
                                      document.getElementById('logout-form').submit();" style="color: red;">
                         {{ __('Logout') }}
                     </a>

                     <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                         @csrf
                     </form>
                    </li>
                @endguest
            </ul>
        </div>
    </div>
  </nav>
</div>
<body>
    @yield('content')
<footer>
    <div class="creditimg" id="creditimg"></div>
    <a href="location" class="lcimg">
        <div class="locationimg"></div>
    </a>
</footer>
</body>
</html>