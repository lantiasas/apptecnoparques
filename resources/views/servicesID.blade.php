@extends('template')
@extends('headermenu')
<link rel="stylesheet" href="{{asset('plugins/servicesET.css')}}">
<link rel="stylesheet" href="{{asset('collapse/accordion.css')}}">
<style type="text/css">
    @media screen and (max-width: 992px) {
      .accordioncenter::after{
        margin: -29px 25px 0px 0px;
      }
    }
    @media (min-width: 1024px) and (max-width: 1680px) {
      .accordioncenter::after{
        margin: -49px 470px 19px 34px;
      }
    }
  </style>
@section('tittle')
    Tecnoparques sena
@endsection

@section('header')
<div class="header">
  <a href="ingd">
  <p class="tittleHeder">
    &#8592; Servicios
  </p>
</a>
</div>
@endsection

@section('content')

<div class="ContentItems">

    <div class="itemtl">
        <p class="tittlePr">
            <br>
            El tecnoparque brinda de forma gratuita asesoría técnica personalizada para:
            <br>
        </p>
        <img src="images/masicons-01.png" class="imgtl">
        <p class="text-justify tittleimg">
            Desarrollo de<br>proyectos en<br>I+D+I
        </p>
        <img src="images/masicons-02.png" class="imgtl1">
        <p class="text-justify tittleimg1">
            Adaptación y<br>transferencia<br>de tecnología
        </p>
        <img src="images/masicons-03.png" class="imgtl2">
        <p class="text-justify tittleimg2">
            Generación de<br>apropiación del<br>conocmiento en<br>una o más de las<br>capacidades
        </p>
        <img src="images/masicons-04.png" class="imgtl3">
        <p class="text-justify tittleimg3">
            Sevicios de las<br>diferentes sub<br>líneas
        </p>
    </div>
    <div class="triangle"></div>
</div>

<div class="ContentItems_">
    @php($count=0)
    @php($count1=0)
    @php($count2=0)
    @php($count3=0)
    @foreach ($servicesID as $item)

        @if($item->id == '42' || $item->id == '48' || $item->id == '51' || $item->id == '56')
        <div class="item">
            <button class="accordioncenter">
                <div class="centertetxt">
                    {{$item->name}}
                </div>
                <hr class="lineP1">
            </button>
            <div class="panelsv">
                <br>
                <div class="textsrv">
        @endif
        
        @if($item->id >=43 && $item->id<=47)
            {{ $item->name }}<br>
            @php($count++)
        @endif

        @if($item->id >=49 && $item->id<=50)
            {{ $item->name }}<br>
            @php($count1++)
        @endif

        @if($item->id >=52 && $item->id<=55)
            {{ $item->name }}<br>
            @php($count2++)
        @endif

        @if($item->id >=57 && $item->id<=58)
            {{ $item->name }}<br>
            @php($count3++)
        @endif

        @if($count==5)
        <br>
            </div>
            </div>
            </div>
            @php($count=0)
        @endif
        
        @if($count1==2)
        <br>
            </div>
            </div>
            </div>
            @php($count1=0)
        @endif
        @if($count2==4)
        <br>
            </div>
            </div>
            </div>
            @php($count2=0)
        @endif
        @if($count3==2)
        <br>
            </div>
            </div>
            </div>
            @php($count3=0)
        @endif

    @endforeach
</div>
<script src="{{asset('collapse/accordioncenter.js')}}"></script>
<script src="{{asset('js/servicesid.js')}}"></script>
@endsection