@extends('templatetec')
@extends('headermenu')
<link rel="stylesheet" href="{{asset('plugins/selectret.css')}}">
@section('headerTittle')
<a href="viewchallenge" style="color: white;">
    Retos
</a>
@endsection

@section('content')
<div class="ContentItemsin">
  <div class="PrTittle"></div>
  <div class="triangle"></div>
  <div class="itemin">
    <div class="d-flex">
      <img src="images/verretos.png" class="imageretinleft">
      <p class="font-weight-bold text-center prretleft">
          Ver retos
      </p>
    </div>
  </div>
  
  <div class="subtittle">
    @php
    $nm = '';
    if($viewchl[0]->nr == ''){
        $nm = 'Reto #'.$viewchl[0]->id;
    }else {
        $nm = $viewchl[0]->nr;
    }
    @endphp
    <div class="row">
      <div class="col-6">
        <p class="title">
          {{$nm}}
        </p>
      </div>
      <div class="col-6">
        <p class="titlename">
          {{$viewchl[0]->name}}
        </p>
      </div>
    </div>
    <hr class="lborder">
  </div>

  <div class="content">
    <p class="sbtittle">
      Descripción
    </p>
    <div class="itemcontent">
      <p class="text-justify">
        {{$viewchl[0]->description}}
      </P>
    </div>
  </div>

  <div class="content">
    <p class="sbtittle">
      Alcance
    </p>
    <div class="itemcontent">
      <p class="text-justify">
        {{$viewchl[0]->scope}}
      </P>
    </div>
  </div>
  
  <div class="content">
    <p class="sbtittle">
      Fecha limite
    </p>
    <div class="itemcontent">
      <p class="text-justify">
        {{$viewchl[0]->deadline}}
      </P>
    </div>
  </div>

  <div class="content">
    <p class="sbtittle">
      Restricciones
    </p>
    <div class="itemcontent">
      <p class="text-justify">
        @php
        for ($i = 0; $i < count($viewchl); $i++)
        echo '- '.$viewchl[$i]->rest.'<br>';
        @endphp
      </P>
    </div>
  </div>

  <div class="content">
    <p class="sbtittle">
      Condiciones
    </p>
    <div class="itemcontent">
      <p class="text-justify">
        @php
        for ($i = 0; $i < count($viewchl1); $i++)
        echo '- '.$viewchl1[$i]->cond.'<br>';
        @endphp
      </P>
    </div>
  </div>

  <div class="content">
    <p class="sbtittle">
      Contactos
    </p>
    <div class="itemcontent">
      <p class="text-justify">
        @php
        for ($i = 0; $i < count($viewchl2); $i++)
          echo '<div>Nombre: '.$viewchl2[$i]->name.'<br>'.
               'Email: '.$viewchl2[$i]->email.'<br>'.
               'Teléfono: '.$viewchl2[$i]->telephone.'<br>'.
               'Posición: '.$viewchl2[$i]->position.'<br>
               <hr class="separtor">
               </div>';
        @endphp
      </P>
    </div>
  </div>

  <div class="contentsl">
    @php
    if($data=='administrator' && $viewchl[0]->approved == '0'){
    echo'
    <form action="aprsln" method="post" enctype="multipart/form-data" id="frmaprbsln">';
    }
    if($data=='administrator' && $viewchl[0]->approved == '1'){
    echo'
    <form action="viewslnt" method="post" enctype="multipart/form-data" id="frmviewslnt">';
    }
    @endphp
    {{@csrf_field()}}
    <input type="hidden" name="idviewchl" value="{{$viewchl[0]->id}}">
      @php
      if($data=='administrator' && $viewchl[0]->approved == '0'){
      echo'
        <button onclick="aprsln(event)" style="border: none;" class="btnsz">
          <div class="itemsln">
            <p class="prsln">Aprobar</p>
            <p class="prsln" style="margin:-63px 0px 0px 280px;">></p>
          </div>
        </button>';
      }
      if($data=='administrator' && $viewchl[0]->approved == '1'){
      echo'
        <button type="submit" style="border: none;" class="btnsz">
          <div class="itemsln">
            <p class="prsln">Ver soluciones</p>
            <p class="prsln" style="margin:-63px 0px 0px 280px;">></p>
          </div>
        </button>';
      }
      @endphp

      @php
      if(($data=='concurrent' && $viewchl[0]->approved == '1') && ($viewchl[0]->idusr != $idlg)){
      echo'
      <form action="slnadd" method="post" enctype="multipart/form-data" id="frmselectchl">';
      }
      if(($data=='concurrent' && $viewchl[0]->approved == '1') && ($viewchl[0]->idusr == $idlg)){
      echo'
      <form action="viewslnt" method="post" enctype="multipart/form-data" id="frmviewslnt">';
      }
      @endphp
        {{@csrf_field()}}
        <input type="hidden" name="idviewchl" value="{{$viewchl[0]->id}}">
        @php
        if(($data=='concurrent' && $viewchl[0]->approved == '1') && ($viewchl[0]->idusr != $idlg)){
        echo'
          <button type="submit" style="border: none;">
            <div class="itemsln">
              <p class="prsln">Agregar solución</p>
              <p class="prsln" style="margin:-63px 0px 0px 280px;">></p>
            </div>
          </button>';
        }

        if(($data=='concurrent' && $viewchl[0]->approved == '1') && ($viewchl[0]->idusr == $idlg)){
        echo'
        <button type="submit" class="btnchlc">
          <div class="itemsln">
            <p class="prsln">Ver soluciones</p>
            <p class="prsln" style="margin:-63px 0px 0px 280px;">></p>
          </div>
        </button>';
      }else {
        echo'<div class="btnchlc">
          <div class="itemsln">
          </div>
          </div>';
      }
        @endphp
    </form>
  </div>
  <div class="lctdw">
    @isset($files[0]->id_challenge)
    <a href="{{ route('createzipchl',['volume_id'=>$files[0]->id_challenge]) }}">Descargar adjuntos</a>
    @endisset
  </div>
  <script src="{{asset('js/chl.js')}}"></script>
</div>
@endsection