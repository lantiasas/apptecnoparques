@extends('template')
@extends('headermenu')
<link rel="stylesheet" href="{{asset('plugins/biotec.css')}}">
@section('tittle')
    Tecnoparques sena
@endsection

@section('header')

<div class="header">
  <a href="helpnextIng">
    <p class="tittleHeder">
      &#8592; Ingeniería y Diseño
    </p>
</a>
</div>
@endsection

@section('content')
<div class="ContentItems">
  <div class="item">
    <img src="images/masicons-16-04.png" class="image">
  </div>

  <a href="location">
    <div class="linkTc">
    </div>
  </a>

  <a href="tecpark?id=3">
    <div class="linkSB">
    </div>
  </a>

  <a href="sublinesID">
    <div class="linklc">
    </div>
  </a>

  <a href="InfrastructureID?id=2">
    <div class="linkin">
    </div>
  </a>

  <a href="servicesID">
    <div class="linkpr">
    </div>
  </a>
    
  <a href="project?id=3">
    <div class="linksv">
    </div>
  </a>

  <a href="helpnextIng">
    <div class="linkbi">
    </div>
  </a>
  
</div>
<script src="{{asset('js/ingd.js')}}"></script>
@endsection