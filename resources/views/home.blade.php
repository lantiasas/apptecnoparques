@extends('templatetec')
<link rel="stylesheet" href="{{asset('plugins/home.css')}}">
<a href="{{ url()->previous() }}">
@section('headerTittle')
Módulos
@endsection
</a>
@section('content')
<div class="ContentItemsin">

  <div class="itemin">
    <img src="images/innovacionabierta.png" class="imagein">
    <p class="font-weight-bold text-center imageTittlein" >
        Innovación<br>abierta
    </p>
  </div>

  <div class="subitemin">
    <div class="btnin">
        <a href="innv">
            <p class="pin">
                Conoce más
            </p>
        </a>
    </div>
  </div>
</div>
<div class="itemin">
</div>
@endsection