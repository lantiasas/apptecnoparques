<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, user-scalable=no">

    <link rel="stylesheet" href="{{asset('bootstrap/css/bootstrap.css')}}">
    <link rel="stylesheet" href="{{asset('plugins/menu.css')}}">
    <link rel="stylesheet" href="{{asset('plugins/size.css')}}">
    <script src="{{asset('plugins/menu.js')}}"></script>

    <link rel="stylesheet" href="{{asset('plugins/fonts.css')}}">
    
    <title>@yield('tittle', 'default')</title>
</head>
    @yield('header')
<body>
</body>
<footer class="footerlogo">
    <div class="creditimg" id="creditimg"></div>
    <a href="location" class="lcimg">
        <div class="locationimg"></div>
    </a>
</footer>
</html>