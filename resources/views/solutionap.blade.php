@extends('templatetec')
@extends('headermenu')
<link rel="stylesheet" href="{{asset('plugins/solutionap.css')}}">
@section('headerTittle')
<a href="{{route('viewslnt',['idviewchl='.$solutionsl[0]->id_challenge.''])}}" style="color: white;">
    Soluciones
</a>
@endsection

@section('content')
<div class="ContentItemsin">
  <div class="PrTittle"></div>
  <div class="triangle"></div>
  <div class="itemin">
    <img src="images/verretos.png" class="imageretinleft">
    <p class="font-weight-bold text-center prretleft" >
        Ver solución
    </p>
  </div>
  
  <div class="subtittle">
    <div class="row">
      <div class="col-6">
        <p class="title">
          {{'Solución #'.$solutionsl[0]->id}}
        </p>
      </div>
      <div class="col-6">
        <p class="titlesl">
          {{$solutionsl[0]->username}}
        </p>
      </div>
    </div>
    <hr class="lborder">
  </div>

  <div class="content">
    <p class="sbtittle">
      Descripción
    </p>
    <div class="itemcontent">
      <p class="text-justify">
          <div class="contentsb">
            {{$solutionsl[0]->description}}
        </div>
      </P>
    </div>
  </div>

  <div class="contentbtn">
    <div class="btngrp">
      @php
          if($solutionsl[0]->approved == '1'){
            echo '<button class="Link1">aprobada</button>';
          }else {
            echo '<button onclick="aprslnt(event)"class="Link2">Aprobar</button>';
          }
      @endphp
      
   
      <a href="{{route('viewslnt',['idviewchl='.$solutionsl[0]->id_challenge.''])}}">
        <button class="Link3">Ver soluciones</button>
      </a>
      <a href="{{route('viewchallenge')}}">
        <button  type="submit" class="Link4">Ver retos</button>
        <form action="{{route('apbslnt',['idchl='.$solutionsl[0]->id.''])}}" method="post" enctype="multipart/form-data" id="frmaprbslnt">
          {{@csrf_field()}}
        </form>
      </a>
      @isset($files[0]->id_solution)
      <a href="{{ route('create-zip',['volume_id'=>$solutionsl[0]->id]) }}">Descargar adjuntos</a>
      @endisset
      
    </div>
  </div>
  <div class="contentbtn">
  </div>
  <script src="{{asset('js/chl.js')}}"></script>
</div>
@endsection