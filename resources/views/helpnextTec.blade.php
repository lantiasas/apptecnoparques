@extends('template')
@extends('headermenu')
<link rel="stylesheet" href="{{asset('plugins/helpnext.css')}}">
@section('tittle')
    Tecnoparques sena
@endsection

@section('header')
<div class="header">
  <a href="help">
    <p class="tittleHeder">
      &#8592; Cómo te podemos ayudar
    </p>
  </a>
</div>
@endsection

@section('content')
<div class="ContentItems">

  @foreach ($tec as $item)
  @if($item->id== '4')
  <div class="item">
    <a href="tecv" style="text-decoration: none;">
      <img src="images/P2-11.png" class="image">
      <p class="font-weight-bold text-center imageTittle" >
          {{$item->name}}
      </p>
    </a>
  </div>

  <div class="item">
    <p class="text-justify P">
      {{$item->description}}
    </p>
  </div>

  @endif
@endforeach

  <!--<div class="item">
    <div class="lct">
        <a href="tecv">
            <p style="font-size:23px; color:#ffffff; margin-top:4px; ">
                Conoce más
            </p>
        </a>
    </div>
  </div>-->
</div>
<script src="{{asset('js/helptec.js')}}"></script>
@endsection