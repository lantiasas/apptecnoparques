@extends('template')
@extends('headermenu')
<link rel="stylesheet" href="{{asset('collapse/accordion.css')}}">
<link rel="stylesheet" href="{{asset('plugins/tecpark.css')}}">
<style type="text/css">
  @media screen and (max-width: 992px) {
    .accordioncenter::after{
      margin: -28px 65px 0px 0px;
    }
  }
  @media (min-width: 1024px) and (max-width: 1680px) {
    .accordioncenter::after{
      margin: -49px 160px 19px 34px;
    }
  }
</style>
@section('tittle')
    Tecnoparques sena
@endsection

@section('header')
<div class="header">
  <a href="{{ url()->previous() }}">
    <p class="tittleHeder">
      &#8592; Tecnoparques
    </p>
  </a>
</div>
@endsection

@section('content')

<div class="PrTittle">
  <p class="text-justify">
      Conozca los Tecnoparques donde se
      cuenta con la línea biotecnología y
      nanotecnología
  </p>
</div>
<div class="triangle"></div>
<div class="ContentItems">
  @foreach ($teckPark as $item)
  <div class="item">
    <button class="accordioncenter">
      <div class="centertetxt">
        {{$item->name}}
      </div>
      <hr class="lineP1" style="text-align: center;">
    </button>
    <div class="panelsv">
      <br>
      <div style="text-align: left;">
        <div class="locationinf"></div>
      </div>
      <div class="inflocation">
        <p class="text-justify P1">
          {{$item->formation_center}} {{$item->address}}
        </p>
        <p class="text-justify paragraphItem">
          {{$item->description}}
            <br><br>
            Teléfono: {{$item->phone}}
        </p>
      </div>
    </div>
  </div>
  @endforeach
</div>
<div style="height:50px;">
</div>
<script src="{{asset('collapse/accordioncenter.js')}}"></script>
<script src="{{asset('js/tecpark.js')}}"></script>
@endsection