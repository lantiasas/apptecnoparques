@extends('template')
<link rel="stylesheet" href="{{asset('plugins/userRegister.css')}}">
@section('tittle')
    Tecnoparques sena
@endsection

@section('header')
<div class="header">

  <div class="imgclose">
    <a href="start" style="text-decoration: none;">
      <div class="initcls">
      </div>
    </a>
  </div>
  <a href="menu">
    <p class="tittleHeder">
      &#8592; Regístrarse
    </p>
  </a>
</div>
@endsection
@section('content')
<a href="menu">
  <div class="cross">
    &#10005;
  </div>
</a> 
<p class="Tittle">
  Llena el formulario
</p>
<div class="triangle_"></div>
<form action="Userregister" method="POST">
  @csrf
  <div class="ContentItems">
    
    <div class="item">
      <p class="tittleItem">Nombre</p>
      <input type="text" id="name" name="name" class="inputStyle">
    </div>

    <div class="item">
      <p class="tittleItem">Correo</p>
      <input type="text" id="email" name="email" class="inputStyle">
    </div>

    <div class="item">
      <p class="tittleItem">Ciudad</p>
      <input type="text" id="city" name="city" class="inputStyle">
    </div>

    <div class="item">
      <p class="tittleItem">Departamento</p>
      <input type="text" id="departament" name="departament" class="inputStyle">
    </div>

  </div>

  <div class="ContentItems">
    <!--<a href="Userregisternext" >
      <div class="Link">Siguiente ></div>
    </a>-->
    <div class="slideshow-container">
      <div class="mySlides">
        <button type="submit" class="Link">Siguiente ></button>
      </div>
    </div>

    <div style="text-align:center; margin-top:20px;">
      <span class="dot on" onclick="currentSlide(1)"></span>
      <a href="Userregisternext">
        <span class="dot off" onclick="currentSlide(2)"></span>
      </a>
      <span class="dot off" onclick="currentSlide(3)"></span> 
    </div>
  </div>

</form>

<link rel="stylesheet" href="{{asset('sliderI/sld.css')}}">
<script src="{{asset('sliderI/sld.js')}}"></script>
@endsection