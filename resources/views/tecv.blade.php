@extends('template')
@extends('headermenu')
<link rel="stylesheet" href="{{asset('plugins/biotec.css')}}">
@section('tittle')
    Tecnoparques sena
@endsection

@section('header')

<div class="header">
  <a href="helpnextTec">
    <p class="tittleHeder">
      &#8592; Tecnologías Virtuales
    </p>
</a>
</div>
@endsection

@section('content')
<div class="ContentItems">
  <div class="item">
    <img src="images/masicons-16-05.png" class="image">
  </div>

  <a href="location">
    <div class="linkTc">
    </div>
  </a>

  <a href="tecpark?id=4">
    <div class="linkSB">
    </div>
  </a>

  <a href="sublinesTV">
    <div class="linklc">
    </div>
  </a>

  <a href="InfrastructureTV">
    <div class="linkin">
    </div>
  </a>

  <a href="servicesTV">
    <div class="linkpr">
    </div>
  </a>
    
  <a href="project?id=4">
    <div class="linksv">
    </div>
  </a>

  <a href="helpnextTec">
    <div class="linkbi">
    </div>
  </a>
</div>
<script src="{{asset('js/tecv.js')}}"></script>
@endsection