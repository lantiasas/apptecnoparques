@extends('templatetec')
@extends('headermenu')
<link rel="stylesheet" href="{{asset('plugins/solutionapusr.css')}}">
@section('headerTittle')
<a href="{{route('viewslnt',['idviewchl='.$solutionsl[0]->id_challenge.''])}}" style="color: white;">
    Soluciones
</a>
@endsection

@section('content')
<div class="ContentItemsin">
    <div class="PrTittle"></div>
    <div class="triangle"></div>
  <div class="itemin">
    <p class="font-weight-bold text-center prretleft">
        
    </p>
  </div>
  
  <div class="subtittle">
    <div class="row">
      <div class="col-6">
        <p class="title">
          {{'Solución #'.$solutionsl[0]->id}}
        </p>
      </div>
      <div class="col-6">
        <p class="title">
          {{$solutionsl[0]->username}}
        </p>
      </div>
    </div>
    <hr class="lborder">
  </div>

  <div class="content">
    <p class="sbtittle">
      Descripción
    </p>
    <div class="itemcontent">
      <p class="text-justify">
          <div class="content">
            {{$solutionsl[0]->description}}
        </div>
      </P>
    </div>
  </div>

  <div class="contentbtn">
    <div class="btngrp">   
      <a href="{{route('viewslnt',['idviewchl='.$solutionsl[0]->id_challenge.''])}}">
        <button class="Link1">Soluciones aprobadas</button>
      </a>
      <a href="{{route('viewchallenge')}}">
        <button  type="submit" class="Link1">Ver retos</button>
        <form action="{{route('apbslnt',['idchl='.$solutionsl[0]->id.''])}}" method="post" enctype="multipart/form-data" id="frmaprbslnt">
          {{@csrf_field()}}
        </form>
      </a>
      @isset($files[0]->id_solution)
      <a href="{{ route('create-zip',['volume_id'=>$solutionsl[0]->id]) }}">Descargar adjuntos</a>
      @endisset
    </div>
  </div>
  <input type="hidden" id="backurlsl" value="{{route('viewslnt',['idviewchl='.$solutionsl[0]->id_challenge.''])}}">
  <script src="{{asset('js/chl.js')}}"></script>
  <script src="{{asset('js/solutionapusr.js')}}"></script>
</div>
@endsection