@extends('template')
<link rel="stylesheet" href="{{asset('plugins/Userregisternext.css')}}">
@section('tittle')
    Tecnoparques sena
@endsection

@section('header')
<div class="header">
    <div class="imgclose">
        <a href="start" style="text-decoration: none;">
          <div class="initcls">
          </div>
        </a>
      </div>
    <a href="Userregister">
        <p class="tittleHeder">
        &#8592; Regístrarse
        </p>
    </a>
</div>
@endsection

@section('content')
<a href="menu">
    <div class="cross">
        &#10005;   
    </div>
</a> 
<p class="Tittle">
    Llena el formulario
</p>
<div class="triangle_"></div>
<form action="Userregisternext" method="POST">
    @csrf
<div class="ContentItems">
    <div class="item">
        <p class="tittleItem">¿Cómo se enteró de la Red Tecnoparque?</p>
        <select id="redPrak" name="redPark" class="selectOption">
            <option value="selectedop">Seleccione una opción</option>
            <option value="Internet">Internet</option>
            <option value="Referido">Referido</option>
            <option value="Evento Tecnoparque">Evento Tecnoparque</option>
            <option value="Centro de Formación SENA">Centro de Formación SENA</option>
            <option value="Universidad">Universidad</option>
            <option value="Empresa">Empresa</option>
            <option value="Otro">Otro:</option>
        </select>
    </div>

    <div class="item">
        <p class="tittleItem">Tipo de afiliación</p>
        <select id="typemembership" name="typemembership" class="selectOption">
            <option value="selectedsop">Seleccione una opción</option>
            <option value="Persona Natural">Persona Natural</option>
            <option value="Emprendedor">Emprendedor</option>
            <option value="Empresario">Empresario</option>
            <option value="Estudiante">Estudiante</option>
            <option value="Empleado">Empleado</option>
        </select>
    </div>

    <div class="item">
        <p class="tittleItem">Tamaño de empresa</p>
        <div class="itemsRadio">
            <div class="custom-control custom-radio custom-control-inline">
                <input type="radio" class="custom-control-input" value="micro" name="typecompany" id="micro">
                <label class="custom-control-label" for="micro">Micro</label>
            </div>
            <div class="custom-control custom-radio custom-control-inline">
                <input type="radio" class="custom-control-input" value="small" name="typecompany" id="samall">
                <label class="custom-control-label" for="samall">Pequeña</label>
                <span class="checkmark"></span>
            </div>
            <div class="custom-control custom-radio custom-control-inline">
                <input type="radio" style="" class="custom-control-input" value="big" name="typecompany" id="big" checked>
                <label class="custom-control-label" for="big">Grande</label>
            </div>
        </div>
    </div>

    <div class="item">
        <!--<div class="link">
            <a href="Userregister">
                <p class="itemLink" style="float:left;margin-left:15px;"> &#x3C; Atrás</p>
            </a>
            <a href="#" type="submit">
                
                <p class="itemLink" style="float:right;margin-right:15px;">Siguiente &#x3E;</p>
            </a>
        </div>
        <button type="submit" class="Link">Siguientea ></button>-->
        <div class="link">
            <div class="slideshow-container">
                <div class="mySlides">
                    <a href="Userregister">
                        <p class="itemLink" style="float:left;margin-left:15px;"> &#x3C; Atrás</p>
                    </a>
                    <a href="Userregister">
                        <button type="submit" class="btnlnk">Siguiente ></button>
                    </a>
                </div>
            </div>
        </div>

        <div style="text-align:center; margin-top:40px;">
            <a href="Userregister">
                <span class="dot off" onclick="currentSlide(1)"></span>
            </a>
            <span class="dot on" onclick="currentSlide(2)"></span>
            <a href="Userregisterend">
                <span class="dot off" onclick="currentSlide(3)"></span>
            </a>
        </div>
    </div>
</div>
</form>
<link rel="stylesheet" href="{{asset('sliderI/sld.css')}}">
<script src="{{asset('sliderI/sld.js')}}"></script>
@endsection