@extends('template')
@extends('headermenu')
<link rel="stylesheet" href="{{asset('plugins/sublines.css')}}">
@section('tittle')
    Tecnoparques sena
@endsection

@section('header')
<div class="header">
  <a href="elect">
    <p class="tittleHeder">
      &#8592; Sublíneas
    </p>
  </a>
</div>
@endsection

@section('content')
<div class="ContentItems">

  <div class="itemtl">
    <p class="tittlePr">
        <br>
        Conoce las sublíneas de trabajo
        <br>
    </p>
  </div>
  <div class="triangle"></div>
  @foreach ($sublinesET as $item)
    <div class="item">
      <p class="font-weight-bold imageTittle" >
        {{$item->name}}
      </p>
      @if($item->icon != '')
        <img src="images/{{$item->icon}}" class="image">
      @endif
    </div>
  @endforeach

</div>
<script src="{{asset('js/sublineset.js')}}"></script>
@endsection