@extends('template')
@section('tittle')
    Tecnoparques sena
@endsection

@section('content')
<body id="imgBack" onload = "linkImg();">
  <header class="" style="background-color: #098A82; padding: 1px; margin:-8px; text-align: center; font-size: 35px; height: 57px; color: white;">
    <div style="font-weight: bold;">
      <a id="lnk">
        <span style='margin-left:-70%; top:-70%;font-size:20px;color:#FFFFFF;'>&#8592;</span>
      </a>
        <div id = "lkstr" style='margin-left:0%; margin-top:-9%;font-size:18px;'></div>
    </div>
  </header>
  </div>
  <div style="border: 10px solid rgb(248, 248, 248); background:rgba(14, 14, 14, 0.40); height: 170px; width: 170px; display: block; position: absolute; left: 50%; top: 50%; border-radius: 50%; transform: translate(-50%, -50%);"></div>
  <div id="tittle" class="" style ="color:#FFFFFF; font-weight: bold; font-size:15px; position: absolute; left: 50%; top: 50%; transform: translate(-50%, -50%);text-align: center;"></div>
  <a id="SectorRadar">
    <div style="border-radius: 50%; background:rgba(14, 14, 14, 0.40); height: 80px; width: 80px; display: block; position: absolute; left: 50%; top: 50%; transform: translate(-50%, 120%);">
      <div class="" style ="color:#FFFFFF; font-weight: bold; font-size:2vw; position: absolute; left: 50%; top: 50%; transform: translate(-50%, -50%);text-align: center;">RADAR</div>
    </div>
  </a>

  <a id="SectorDrivers">
    <div style="background:rgba(14, 14, 14, 0.40); height: 80px; width: 80px; display: block; position: absolute; left: 50%; top: 50%; border-radius: 50%; transform: translate(-50%, -220%);">
      <div class="" style ="color:#FFFFFF; font-weight: bold; font-size:2vw; position: absolute; left: 50%; top: 50%; transform: translate(-50%, -50%);text-align: center;">DRIVERS</div>
    </div>
  </a>

  <a id="Milestones">
    <div style="background:rgba(14, 14, 14, 0.40); height: 80px; width: 80px; display: block; position: absolute; left: 50%; top: 50%; border-radius: 50%; transform: translate(-205%, -120%);">
      <div class="" style ="color:#FFFFFF; font-weight: bold; font-size:2vw; position: absolute; left: 50%; top: 50%; transform: translate(-50%, -50%);text-align: center;">HITOS</div>
    </div>
  </a>
  <a id="SectorResearchGroups">
    <div style="background:rgba(14, 14, 14, 0.40); height: 80px; width: 80px; display: block; position: absolute; left: 50%; top: 50%; border-radius: 50%; transform: translate(-205%, 20%);">
      <div class="" style ="color:#FFFFFF; font-weight: bold; font-size:2vw; position: absolute; left: 50%; top: 50%; transform: translate(-50%, -50%);text-align: center;">GRUPOS</div>
    </div>
  </a>
  <a id="videos">
    <div style="background:rgba(14, 14, 14, 0.40); height: 80px; width: 80px; display: block; position: absolute; left: 50%; top: 50%; border-radius: 50%; transform: translate(105%, 25%);">
      <div class="" style ="color:#FFFFFF; font-weight: bold; font-size:2vw; position: absolute; left: 50%; top: 50%; transform: translate(-50%, -50%);text-align: center;">VIDEOS</div>
    </div>
  </a>
  <a id="SectorReferences">
    <div style="background:rgba(14, 14, 14, 0.40); height: 80px; width: 80px; display: block; position: absolute; left: 50%; top: 50%; border-radius: 50%; transform: translate(105%, -118%);">
      <div class="" style ="color:#FFFFFF; font-weight: bold; font-size:2vw; position: absolute; left: 50%; top: 50%; transform: translate(-50%, -50%);text-align: center;">REFERENTES</div>
    </div>
  </a>
</body>
<script src="{{asset('js/items.js')}}"></script>
@endsection