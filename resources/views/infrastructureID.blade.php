@extends('template')
<link rel="stylesheet" href="{{asset('plugins/Infrastructure.css')}}">
@section('tittle')
    Tecnoparques sena
@endsection

@section('header')
<div class="header">
  <a href="{{ url()->previous() }}">
    <p class="tittleHeder">
        &#8592; Infraestructura
    </p>
  </a> 
</div>
@endsection

@section('content')
<div class="ContentItems">

    @foreach ($InfrastructureID as $item)
    <div class="item">
        <p class="text-center IngTittle">
        {{$item->name}}
        </p>
    </div >
    @endforeach
</div>
@endsection