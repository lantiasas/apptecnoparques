@extends('template')
<link rel="stylesheet" href="{{asset('plugins/modal.css')}}">
<style type="text/css">
    @media (min-width: 1024px) and (max-width: 1680px) {
      .tittleMenu, .sidebarIconToggle{
          display:none !important;
      }
    }
</style>
@section('tittle')
    Tecnoparques sena
@endsection

@section('content')
<div class="header">
  <div class="imgclose">
    <a href="start" style="text-decoration: none;">
      <div class="initcls">

      </div>
    </a>
  </div>
  <div class="helpnextTec">
    <p class="tittleMenu">
      Módulos
    </div>
</div>
<input type="checkbox" class="openSidebarMenu" id="openSidebarMenu">
  <label for="openSidebarMenu" class="sidebarIconToggle">
    <div class="spinner diagonal part-1"></div>
    <div class="spinner horizontal"></div>
    <div class="spinner diagonal part-2"></div>
  </label>
  <div id="sidebarMenu">
    <ul class="sidebarMenuInner">
      <div class="logo-image">
        <img src="images/Tecnoparque_Mesa de trabajo1.png" class="img-fluid imageMenu">
      </div>
    </ul>
    <div class="file">
      <div class="itemsMenuB">
        <a href="start">
          <p class="itemsB">Bienvenidos ></p>
          <hr class="borderLine" style="margin:-26px 0px 0px 47px;">
        </a>
      </div>
      <div class="itemsMenuM">
        <a href="">
          <p class="itemsM">Módulos ></p>
          <hr class="borderLine" style="margin:-26px 0px 0px 47px;">
        </a>
      </div>
      <div class="itemsMenuR">
        <a href="Userregister">
          <p class="itemsR">Registro ></p>
            <p class="text-justify paragraph">
              Regístrate para recibir información
              de convocatorias y oportunidades de
              la Red Tecnoparques Sena.
            </p>
          <hr class="borderLine" style="margin:-20px 0px 0px 47px;">
        </a>
      </div>
      <div class="itemsMenuC">
        <a href="credits">
          <p class="itemsC">Créditos ></p>
        </a>
      </div>
    </div>
  </div>
</div>

<div class="itemsImage">
  <div class="imageContent">
    <a href="help">
      <img src="images/IMG-01.jpg" class="img-responsive sizeImage">
      <div class="contentXTH">
        <div class="mySlidesH textSlideH">
          Conoce nuestros<br>Tecnoparques
        </div>
        <!--<div class="mySlidesH textSlideTH">
                      Conoce LOS SERVICIOS QUE SE<br>
                      PRESTAN A TRAVÉS DE LA RED<br>
                      TECNOPARQUE en 4 líneas tecnológicas.<br>
                      Biotecnología y nanotecnología.<br>
                      Electrónica y telecomunicaciones.<br>
                      Ingeniería y diseño.<br>
                      Tecnologías virtuales.<br>  
        </div>
        <a class="prevH" onclick="plusSlides(-1)" style="color:white">❮</a>
        <a class="nextH" onclick="plusSlides(1)" style="color:white">❯</a>-->
        <a class="nexth" data-toggle="modal" id="helpmodal" style="color:white">+</a>

      </div>
      <img src="images/Icono-ConoceComoAyudar-02.png" class="img-responsive sizeIcon">
    </a>
  </div>

  <div class="imageContent">
    <a href="https://gestionredtecnoparquecolombia.com.co/">
      <img src="images/IMG-02.jpg" class="img-responsive sizeImage">
      <div class="contentXTR">
        <div class="mySlidesR textSlideR">
          Registra<br>tu idea
        </div>
        <div class="mySlidesR textSlideTR">
          Tienes una idea para prototipar o validar?.<br>
          Regístrala aquí<br>
        </div>
        <!--<a class="prevR" onclick="plusSlidesr(-1)" style="color:white">❮</a>
        <a class="nextR" onclick="plusSlidesr(1)" style="color:white">❯</a>-->
        <a class="nexth" data-toggle="modal" id="rgmodal" style="color:white">+</a>
      </div>
      <img src="images/Icono-RegistatuIdea-02.png" class="img-responsive sizeIcon">
    </a>
  </div>

  <div class="imageContent">
    <a href="login">
      <img src="images/IMG-03.jpg" class="img-responsive sizeImage">
      <div class="contentXTP">
        <div class="mySlidesP textSlideP">
          Innovación<br>abierta
        </div>
        <div class="mySlidesP textSlideTP">
          Conoce tendencias y tecnologías de<br>
          sectores de clase mundial<br>
        </div>
        <a class="nexth" data-toggle="modal" id="opmodal" style="color:white">+</a>
        <!--<a class="prevP" onclick="plusSlidesp(-1)" style="color:white">❮</a>
        <a class="nextP" onclick="plusSlidesp(1)" style="color:white">❯</a>-->
      </div>
      <img src="images/Icono-InnovacionAbierta-02.png" class="img-responsive sizeIcon">
    </a>
  </div>

  <div id="container">
    
    <div class="inner">
      <div class="img1"></div>
      <div class="img2"></div>
      <div class="img3"></div>
      <div class="imgurl">
        
        <a href="help" style="text-decoration: none;">
          <div class="imglink">
            <div class="textprinfo">
              Conoce nuestros<br>
              Tecnoparques
            </div>
          </div>
        </a>

        <a href="https://gestionredtecnoparquecolombia.com.co/" style="text-decoration: none;">
          <div class="imglink1">
            <div class="textprinfo">
              Registra<br>
              tu idea
            </div>
          </div>
        </a>

        <a href="login" style="text-decoration: none;">
          <div class="imglink2">
            <div class="textprinfo">
              Innovación<br>
              abierta
            </div>
          </div>
        </a>
    </div>
  </div>
</div>

</div>

<div id="myModal" class="modalp">
  <div class="modal-contentp">
    <div class="closep">&#10006;</div>
    <p class="tittle">Volver a Menú</p>
    <div class="imgcnt">
      <img id="imagetl" class="imgc">
      <div id="textct" class="textcnt"></div>
      <div id="textctsb" class="textcntsb"></div>
      <div id="textctits" class="textcntsbit"></div>
    </div>
  </div>
</div>

<link rel="stylesheet" href="{{asset('slide/slideHelp.css')}}">
<script src="{{asset('slide/slideHelp.js')}}"></script>

<link rel="stylesheet" href="{{asset('slide/slideR.css')}}">
<script src="{{asset('slide/slideR.js')}}"></script>

<link rel="stylesheet" href="{{asset('slide/slideP.css')}}">
<script src="{{asset('slide/slideP.js')}}"></script>

<script src="{{asset('js/modal.js')}}"></script>
@endsection