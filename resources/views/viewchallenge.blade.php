@extends('templatetec')
@extends('headermenu')
<link rel="stylesheet" href="{{asset('plugins/viewchallenge.css')}}">
@section('headerTittle')
<a href="innv" style="color: white;">
    Retos
</a>
@endsection

@section('content')
<div class="ContentItemsin">
    <div class="PrTittle"></div>
    <div class="triangle"></div>
    <div class="itemin d-flex flex-column">
        <div class="d-flex flex-row justify-content-around align-items-center" style="position: relative; top: -80px; margin: 0px 770px">
            <img src="images/verretos.png" class="imageretinlefts" style="width: 50px;">
            <p class="font-weight-bold text-white m-0" >
                Ver retos
            </p>
        </div>
    </div>

    <div class="iteminview">
        <div class="tblsize">
            <table class="table">
                @foreach($retos as $value)
                <tr>
                    @php
                        $nm = '';
                        if($value->nr == ''){
                            $nm = 'Reto #'.$value->id;
                        }else {
                            $nm = $value->nr;
                        }
                    @endphp
                    <td class="cnt">{{$nm}}</td>
                    <!--<td>{{$value->description}}</td>-->
                    <td>
                        <form action="viewselectchl" method="post" enctype="multipart/form-data" id="frmviewchl">
                            {{@csrf_field()}}
                            <input type="hidden" name="idchl" value="{{$value->id}}">
                            <button type="submit" class="btnchl">
                                <a href="#">
                                    <img src="images/view.png" class="imglinkview">
                                </a>
                            </button>
                        </form>
                    </td>
                </tr>
                @endforeach
            </table>
        </div>
    </div>
    <div class="iteminview">
        <div class="pgcenter">
            {{ $retos->links() }}
        </div>
    </div>
</div>
<script src="{{asset('js/viewchallenge.js')}}"></script>
@endsection