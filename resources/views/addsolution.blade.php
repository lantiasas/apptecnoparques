@extends('templatetec')
@extends('headermenu')
<link rel="stylesheet" href="{{asset('plugins/addsolution.css')}}">
@section('headerTittle')
<a href="{{route('viewselectchl',['idchl='.$solution[0]->id.''])}}" style="color: white;">
    Retos
</a>
@endsection

@section('content')
<div class="ContentItemsin">
  <div class="PrTittle"></div>
  <div class="triangle"></div>
  <form action="{{route('addsolution')}}" method="post" enctype="multipart/form-data" id="frmaddsolution">
    {{@csrf_field()}}
    <input type="hidden" name="idchls" value="{{$solution[0]->id}}">
  <div class="itemin">
    <img src="../images/soluciones.png" class="imageretinleft">
    <p class="font-weight-bold text-center prretleft">
        Propuesta de solución 
    </p>
  </div>

  <div class="subtittle">
    @php
    $nm = '';
    if($solution[0]->namechl == ''){
        $nm = 'Reto #'.$solution[0]->id;
    }else {
        $nm = $solution[0]->namechl;
    }
    @endphp
    <div class="row">
      <div class="col-6">
        <p class="title">
          {{$nm}}
        </p>
      </div>
      <div class="col-6">
        <p class="tittlesb">
          {{$solution[0]->name}}
        </p>
      </div>
    </div>
    <hr class="lborder">
  </div>

  <div class="content">
    <p class="sbtittle">
      Descripción
    </p>
    <div class="itemcontent">
        <textarea name="descriptionsln" id="descriptionsln" cols="50" rows="11" class="form-control"></textarea>
    </div>
    <div class="itemcontent">
        <p class="sbtittle">
            Adjuntar archivos
        </p>
        <div class="image-upload">
            <label for="file-input">
                <img src="../images/add.png"/>
            </label>
            <input id="file-input" type="file" name="archivessln[]" onchange="uploadArchive(event);" multiple=""/>
        </div>
    </div>
  </div> 
  <div class="itemcontent">
    <div class="btngrp">
        <input type="button" class="Link" onclick="location.href='{{route('viewchallenge')}}';" value="Ver retos"/>
        <input type="button" class="Link" onclick="location.href='{{route('viewselectchl',['idchl='.$solution[0]->id.''])}}';" value="Regresar"/>
        <button onclick="newsolution(event)" class="Link">Guardar</button>
      </div>
  </div>
  </form>
  <input type="hidden" id="url" value="{{route('viewselectchl',['idchl='.$solution[0]->id.''])}}">
  <script src="{{asset('js/addsolution.js')}}"></script>
</div>
@endsection